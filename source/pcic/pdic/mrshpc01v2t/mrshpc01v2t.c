/*
 *  FatFs for TOPPERS
 *      FAT File system/
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 * 
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2005- by Industrial Technology Institute,
 *                          Miyagi Prefectural Government, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 */

/*
 *  丸文MR-SHPC-01 V2TのPDIC
 *　　・活線挿抜には未対応
 *　　・PCカードATAにのみ対応
 *　　・PCカードは1枚のみ（ハードウェア上の制限）
 *　　
 *　　・備考
 *　　　　カードIDは0から始まる番号とする
 */

#include "mrshpc01v2t.h"
#include "mrshpc01v2t_sil.h"
#include "mrshpc01v2t_cfg.h"

/*
 *  レジスタの定義
 */
#define MODE_REG		0xe4u	/*  モードレジスタ  */
#define OPTION_REG		0xe6u	/*  オプションレジスタ  */
#define CARD_STATUS_REG		0xe8u	/*  カードステータスレジスタ  */
#define INT_EVENT_REG		0xeau	/*  割込み要因レジスタ  */
#define INT_CTRL_REG		0xecu	/*  割込み制御レジスタ  */
#define CARD_POW_CTRL_REG	0xeeu	/*  カード電源制御レジスタ  */
#define MEM_WIN0_CTRL_REG1	0xf0u	/*  メモリウィンドウ0コントロールレジスタ1  */
#define MEM_WIN1_CTRL_REG1	0xf2u	/*  メモリウィンドウ1コントロールレジスタ1  */
#define IO_WIN_CTRL_REG1	0xf4u	/*  I/Oウィンドウコントロールレジスタ1  */
#define MEM_WIN0_CTRL_REG2	0xf6u	/*  メモリウィンドウ0コントロールレジスタ2  */
#define MEM_WIN1_CTRL_REG2	0xf8u	/*  メモリウィンドウ1コントロールレジスタ2  */
#define IO_WIN_CTRL_REG2	0xfau	/*  I/Oウィンドウコントロールレジスタ2  */
#define CARD_CTRL_REG		0xfcu	/*  カードコントロールレジスタ  */
#define PCIC_INFO_REG		0xfeu	/*  PCIC情報レジスタ  */

/*  モードレジスタ  */
#define MODE_SH4		BIT5	 /*  RDY信号  */
#define MODE_RDY		MODE_SH4 /*  RDY信号  */
#define MODE_WAIT		0u	 /*  WAIT信号  */
#define MODE66			BIT4	 /*  66MHz対応  */

/*  カード・ステータ・スレジスタ  */
#define STATUS_CD12	(BIT3 | BIT2)	/*  カード検出：CCD2,1端子  */

/*  割込み制御レジスタ  */
#define INT_CTRL_CPGOOD_ENA	BIT4	/*  カード電源変化割込みイネーブル  */
#define INT_CTRL_DETECT_ENA	BIT3	/*  カード検出割込みイネーブル  */
#define INT_CTRL_RDY_ENA	BIT2	/*  CRDY_BSY_IREQ割込みイネーブル  */
#define INT_CTRL_BAT_WAR_ENA	BIT1	/*  バッテリ・ワーニング割込みイネーブル  */
#define INT_CTRL_BAT_DE_ENA	BIT0	/*  バッテリ・デッド割込みイネーブル  */
#define INT_CTRL_ALL_DISABLE	0x0u	/*  すべてディセーブル  */


/*  カード電源制御レジスタ  */
#define CARD_POW_CTRL_CARD_POW_MASK	BIT10	/*  CARD_PW_GOOD端子をマスク  */
#define CARD_POW_CTRL_CARD_RESET	BIT9	/*  カードリセット  */
						/*  　0のとき、アサート  */
#define CARD_POW_CTRL_POWER_DOWN	BIT8	/*  消費電力最小モード  */
#define CARD_POW_CTRL_SUSPEND		BIT7	/*  サスペンド・モード  */
#define CARD_POW_CTRL_CARD_ENA		BIT6	/*  カードへの信号出力イネーブル  */
#define CARD_POW_CTRL_AUTO_POWER	BIT5	/*  挿抜検出Auto Powイネーブル  */
#define CARD_POW_CTRL_VCC_POWER		BIT4	/*  カード電源ON  */
#define CARD_POW_CTRL_VCC5		BIT3	/*  CVCC5端子出力値設定（反転出力）*/
#define CARD_POW_CTRL_VCC3		BIT2	/*  CVCC3端子出力値設定（反転出力）*/
#define CARD_POW_CTRL_VPP1		BIT1	/*  CVPP1端子出力値設定  */
#define CARD_POW_CTRL_VPP0		BIT0	/*  CVPP0端子出力値設定  */
	/*  
	 *　カード電源制御レジスタの初期値
	 *　　　・ビット10:1　CARD_PW_GOODマスク
	 *　　　・ビット9:1　カード・リセット・ネゲート
	 *　　　・ビット8:0　電力：通常モード
	 *　　　・ビット7:0　サスペンド：通常モード
	 *　　　・ビット6:1　カードへの信号出力
	 *　　　・ビット5:0　カード挿抜検出を無視
	 *　　　・ビット4:0　カード電源OFF
	 *　　　・ビット3:0　CVCC5=1
	 *　　　・ビット2:1　CVCC3=0
	 *　　　・ビット1:0　CPP1=0
	 *　　　・ビット0:0　CPP0=0
	 */
#define CARD_POW_CTRL_INI		(CARD_POW_CTRL_CARD_POW_MASK	\
					| CARD_POW_CTRL_CARD_RESET	\
					| CARD_POW_CTRL_CARD_ENA	\
					| CARD_POW_CTRL_VCC3)

/*
 *  メモリウィンドウ0コントロールレジスタ1
 *  メモリウィンドウ1コントロールレジスタ1
 *  I/Oウィンドウコントロールレジスタ1
 */
#define WIN_CTRL1_WINEN		BIT15	/*  ウィンドウ・イネーブル  */
#define WIN_CTRL1_WIDTH		0x7c00u	/*  コマンド・パルス幅時間  */
#define WIN_CTRL1_HOLD		0x0300u	/*  コマンド立ち上がり時間  */
#define WIN_CTRL1_SETUP		0x00c0u	/*  コマンド立ち下がり時間  */
#define WIN_CTRL1_SA25_20	0x003fu	/*  ウィンドウスタートアドレス  */
	/*
	 *  ウィンドウコントロールレジスタ1の初期値
	 *  　　・ウィンドウ・イネーブル
	 *  　　・アクセス・スピード：最も遅いモード
	 *  　　・ウィンドウ・スタートアドレス：ボード依存
	 */
#define WIN_CTRL1_INI	(WIN_CTRL1_WINEN		\
			| WIN_CTRL1_WIDTH		\
			| WIN_CTRL1_HOLD		\
			| WIN_CTRL1_SETUP)

/*
 *  メモリウィンドウ0コントロールレジスタ2
 *  メモリウィンドウ1コントロールレジスタ2
 *  I/Oウィンドウコントロールレジスタ2
 */
#define WIN_CTRL2_SWAP		BIT11	/*  スワップなし  */
#define WIN_CTRL2_WRITE_PRO	BIT10	/*  ライト・プロテクト  */
#define WIN_CTRL2_SIZE		BIT9	/*  データバス：16ビット  */
#define WIN_CTRL2_REG		BIT8	/*  コモンメモリ選択  */
#define WIN_CTRL2_AUTO_SIZE	BIT8	/*  XIOIS16信号でデータバス幅を決定  */
					/*  （I/Oウィンドウコントロールレジスタ2）  */
#define WIN_CTRL2_CA25_18	0x00ffu	/*  カード・アドレス・セット  */
	/*
	 *  メモリ・ウィンドウ・コントロールレジスタ2の初期値
	 *  　　・スワップなし
	 *  　　・書き込み許可
	 *  　　・データバス：8ビット
	 *  　　・アトリビュート・メモリを選択
	 */
#define MEM_WIN_CTRL2_INI	WIN_CTRL2_SWAP
	/*
	 *  I/Oウィンドウ・コントロールレジスタ2の初期値
	 *  　　・スワップなし
	 *  　　・書き込み許可
	 *  　　・データバス：8ビット
	 *  　　・XIOIS16信号に関係なく、データバス幅を決定
	 */
#define IO_WIN_CTRL2_INI	WIN_CTRL2_SWAP


/*  カード・コントロール・レジスタ  */
#define CARD_CTRL_CARD_IS_IO	BIT3	/*  カードモード：I/OカードI/F  */
#define CARD_CTRL_LED_ENABLE	BIT2	/*  LED_OUT端子イネーブル  */
#define CARD_CTRL_SPKR_ENABLE	BIT1	/*  SPKR_OUT端子イネーブル  */
#define CARD_CTRL_INPACK_ENABLE	BIT0	/*  CINPACK信号有効  */

/*
 *  ウィンドウ・スタートアドレスのアドレス範囲チェック用マクロ
 */
#define CHECK_SA25_20(data)	(((data) & ~WIN_CTRL1_SA25_20) != 0u)
#define CHECK_SA25_18(data)	(((data) & ~WIN_CTRL2_CA25_18) != 0u)

/*
 *  ウィンドウ・スタートアドレスのアドレス範囲チェック
 */
#if CHECK_SA25_20(MRSHPC01V2T_MEM_WIN_SA25_20)
#error MRSHPC01V2T_MEM_WIN_SA25_20 is out of range
#endif	/* CHECK_SA25_20(MRSHPC01V2T_MEM_WIN_SA25_20) */

#if CHECK_SA25_18(MRSHPC01V2T_MEM_WIN_CA25_18)
#error MRSHPC01V2T_MEM_WIN_CA25_18 is out of range
#endif	/* CHECK_SA25_18(MRSHPC01V2T_MEM_WIN_CA25_18) */

#if CHECK_SA25_20(MRSHPC01V2T_IO_WIN_SA25_20)
#error MRSHPC01V2T_IO_WIN_SA25_20 is out of range
#endif	/* CHECK_SA25_20(MRSHPC01V2T_IO_WIN_SA25_20) */

#if CHECK_SA25_18(MRSHPC01V2T_IO_WIN_CA25_18)
#error MRSHPC01V2T_IO_WIN_CA25_18 is out of range
#endif	/* CHECK_SA25_18(MRSHPC01V2T_IO_WIN_CA25_18) */

/*  初期化済みフラグ  */
static BOOL 	initialized = FALSE;

/*
 *  PDICの初期化ルーチン
 *　　モード・レジスタの仕様のため、電源投入後、1回しか初期化できない
 */
void
mrshpc01v2t_initialize(void)
{
	mrshpc01v2t_sil_initialize();	/*  バス・コントローラの初期化  */
	
	/*  
	 *　PCICデバイスの初期化
	 */

	/*  
	 *　モード・レジスタの設定
	 *　　電源投入後、一回しか設定できない。
	 */
	if (initialized) {
		syslog_0(LOG_CRIT, "Can\'t initilize PCIC(mrshpc01v2t) twice !");
	}

	initialized = TRUE;
	mrshpc01v2t_wrh_reg(MODE_REG, MRSHPC01V2T_MODE_REG_INI);
	
	/*  PCICからのすべての割込みを禁止  */
	mrshpc01v2t_wrh_reg(INT_CTRL_REG, INT_CTRL_ALL_DISABLE);

	/*  メモリカードI/F選択（まだ、カードには信号出力されない）  */
	mrshpc01v2t_wrh_reg(CARD_CTRL_REG, 0x0u);

	/*  カードへの信号出力を有効化  */
	mrshpc01v2t_wrh_reg(CARD_POW_CTRL_REG, CARD_POW_CTRL_INI);
}

/*
 *  カード検出
 *  　　引数
 *  　　　　ID cardid：カードID
 *  　　戻り値
 *  　　　　BOOL ret:
 *  　　　　　　TRUE ：カードあり
 *  　　　　　　FALSE：カードなし
 */
BOOL 
mrshpc01v2t_detect_card(ID cardid)
{
	UH status;
	
	assert(initialized);
	assert(cardid == 0);	/*  カードIDのチェック  */
	status = mrshpc01v2t_reh_reg(CARD_STATUS_REG);
	status &= STATUS_CD12;
	if (status == 0x0) {
		return TRUE;	/*  カードあり  */
	} else {
		return FALSE;	/*  カードなし  */
	}
}

/*
 *  カード・リセット
 *  　　引数
 *  　　　　ID cardid：カードID
 */
void 
mrshpc01v2t_reset_card(ID cardid)
{
	assert(initialized);
	assert(cardid == 0);	/*  カードIDのチェック  */
	mrshpc01v2t_anh_reg(CARD_POW_CTRL_REG, (UH)~CARD_POW_CTRL_CARD_RESET);
}

/*
 *  カード・リセットをネゲート
 *  　　引数
 *  　　　　ID cardid：カードID
 */
void 
mrshpc01v2t_negate_reset_card(ID cardid)
{
	assert(initialized);
	assert(cardid == 0);	/*  カードIDのチェック  */
	mrshpc01v2t_orh_reg(CARD_POW_CTRL_REG, CARD_POW_CTRL_CARD_RESET);
}

/*
 *  カード電源ON
 *  　　引数
 *  　　　　ID cardid：カードID
 */
void 
mrshpc01v2t_porwer_on_card(ID cardid)
{
	assert(initialized);
	assert(cardid == 0);	/*  カードIDのチェック  */
	mrshpc01v2t_orh_reg(CARD_POW_CTRL_REG, CARD_POW_CTRL_VCC_POWER);
}	

/*
 *  カード電源OFF
 *  　　引数
 *  　　　　ID cardid：カードID
 */
void 
mrshpc01v2t_porwer_off_card(ID cardid)
{
	assert(initialized);
	assert(cardid == 0);	/*  カードIDのチェック  */
	mrshpc01v2t_anh_reg(CARD_POW_CTRL_REG, (UH)~CARD_POW_CTRL_VCC_POWER);
}	

/*
 *  メモリ・ウィンドウ・オープン
 *  　　引数
 *  　　　　ID cardid：カードID
 *  　　
 *  　　備考
 *  　　　　メモリウィンドウ０を使う。
 */
void 
mrshpc01v2t_open_memory_window(ID cardid)
{
	UH reg;
	
	assert(initialized);
	assert(cardid == 0);	/*  カードIDのチェック  */

	reg = WIN_CTRL1_INI | MRSHPC01V2T_MEM_WIN_SA25_20;
	mrshpc01v2t_wrh_reg(MEM_WIN0_CTRL_REG1, reg);
	
	reg = MEM_WIN_CTRL2_INI | MRSHPC01V2T_MEM_WIN_CA25_18;
	mrshpc01v2t_wrh_reg(MEM_WIN0_CTRL_REG2, reg);
	mrshpc01v2t_set_mwb();		/*  システム・バスの設定  */
}

/*
 *  メモリ・ウィンドウ・クローズ
 *  　　引数
 *  　　　　ID cardid：カードID
 */
void 
mrshpc01v2t_close_memory_window(ID cardid)
{
	assert(initialized);
	assert(cardid == 0);	/*  カードIDのチェック  */
	mrshpc01v2t_anh_reg(MEM_WIN0_CTRL_REG1, (UH)~WIN_CTRL1_WINEN);
}

/*
 *  I/Oウィンドウ・オープン
 *  　　引数
 *  　　　　ID cardid：カードID
 */
void 
mrshpc01v2t_open_io_window(ID cardid)
{
	UH reg;
	
	assert(initialized);
	assert(cardid == 0);	/*  カードIDのチェック  */

	reg = WIN_CTRL1_INI | MRSHPC01V2T_IO_WIN_SA25_20;
	mrshpc01v2t_wrh_reg(IO_WIN_CTRL_REG1, reg);
	
	reg = IO_WIN_CTRL2_INI | MRSHPC01V2T_IO_WIN_CA25_18;
	mrshpc01v2t_wrh_reg(IO_WIN_CTRL_REG2, reg);
	mrshpc01v2t_set_iowb();		/*  システム・バスの設定  */
}

/*
 *  I/Oウィンドウ・クローズ
 *  　　引数
 *  　　　　ID cardid：カードID
 */
void 
mrshpc01v2t_close_io_window(ID cardid)
{
	assert(initialized);
	assert(cardid == 0);	/*  カードIDのチェック  */
	mrshpc01v2t_anh_reg(IO_WIN_CTRL_REG1, (UH)~WIN_CTRL1_WINEN);
}


/*
 *  アトリビュート・メモリを選択
 *  　　引数
 *  　　　　ID cardid：カードID
 */
void 
mrshpc01v2t_select_attribute_memory(ID cardid)
{
	assert(initialized);
	assert(cardid == 0);	/*  カードIDのチェック  */
	
	/*  REGビット・クリアでアトリビュートメモリ選択  */
	mrshpc01v2t_anh_reg(MEM_WIN0_CTRL_REG2, (UH)~WIN_CTRL2_REG);
}

/*
 *  I/Oカード・モードに設定する。
 *  　　引数
 *  　　　　ID cardid：カードID
 */
void 
mrshpc01v2t_set_io_card_mode(ID cardid)
{
	assert(initialized);
	assert(cardid == 0);	/*  カードIDのチェック  */
	mrshpc01v2t_orh_reg(CARD_CTRL_REG, CARD_CTRL_CARD_IS_IO);
}

