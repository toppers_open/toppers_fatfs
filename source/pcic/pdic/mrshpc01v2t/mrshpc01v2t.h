/*
 *  FatFs for TOPPERS
 *      FAT File system/
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 * 
 *  Copyright (C) 2005- by Industrial Technology Institute,
 *                          Miyagi Prefectural Government, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 */

/*
 *  丸文MR-SHPC-01 V2TのPDIC
 */
#ifndef _MRSHPC01V2T_H_
#define _MRSHPC01V2T_H_

/*
 *  各ウィンドウ内のオフセット
 */
#define MRSHPC01V2T_REAL_WORD_OFFSET	0x00000u	/*  Real Word領域  */
#define MRSHPC01V2T_REAL_BYTE_OFFSET	0x40000u	/*  Real Byte領域  */
#define MRSHPC01V2T_DUMMY_WORD_OFFSET	0x80000u	/*  Dummy Word領域  */
#define MRSHPC01V2T_DUMMY_BYTE_OFFSET	0xc0000u	/*  Dummy Byte領域  */


#include "mrshpc01v2t_sil.h"

extern void mrshpc01v2t_initialize(void);
extern BOOL mrshpc01v2t_detect_card(ID cardid);
extern void mrshpc01v2t_reset_card(ID cardid);
extern void mrshpc01v2t_negate_reset_card(ID cardid);
extern void mrshpc01v2t_porwer_on_card(ID cardid);
extern void mrshpc01v2t_porwer_off_card(ID cardid);
extern void mrshpc01v2t_open_memory_window(ID cardid);
extern void mrshpc01v2t_close_memory_window(ID cardid);
extern void mrshpc01v2t_open_io_window(ID cardid);
extern void mrshpc01v2t_close_io_window(ID cardid);
extern void mrshpc01v2t_select_attribute_memory(ID cardid);
extern void mrshpc01v2t_set_io_card_mode(ID cardid);

#endif /* _MRSHPC01V2T_H_ */
