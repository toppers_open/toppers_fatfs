/*
 *  FatFs for TOPPERS
 *      FAT File system/
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 * 
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2005- by Industrial Technology Institute,
 *                          Miyagi Prefectural Government, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 */

/*
 *  PCIC(PC card Interface Controler)のGDIC
 *　　・活線挿抜には未対応
 *　　・PCカードATAにのみ対応
 *　　・PCICが1台で、その下にPCカードが複数枚挿入されている
 *　　　ハードウェア構成を想定
 *　　・PCカード2枚まで対応
 *　　　　・配列の大きさのみに依存するので、容易に拡張可能
 *　　・PCICが複数ある場合には未対応
 *　　　　・ハードウェア依存部を1種類しか呼び出せない構造
 *　　　　・どのPCICにソケットがいくつあるかといった情報の管理は
 *　　　　　将来的にコンポーネントWGの成果を取り入れたい
 *　　
 *　　・備考
 *　　　　カードIDは0から始まる番号とする
 */

#include <t_services.h>
#include "kernel_id.h"
#include "pcic.h"
#include "hw_pcic.h"

/*
 *  CIS関連の定義
 *　　CIS：Card Information Structure
 */
#define TMAX_PCIC_TULPE_LENGTH	0x100	/*  タプルの最大長  */
#define TSZ_PCIC_TULPE_AREA	0x100	/*  タプル領域のサイズ  */
					/*  　0x200/2バイト  */
/*  タプルの定義  */
#define CISTPL_NULL		0x00	/*  NULLタプル  */
#define CISTPL_END		0xff	/*  連鎖終端タプル  */

#define CISTPL_LINTARGET	0x13	/*  リンクターゲット制御  */
#define CISTPL_LONGLINK_A	0x11	/*  アトリビュートメモリへのロングリンク  */
#define CISTPL_LONGLINK_C	0x12	/*  コモンメモリへのロングリンク  */
#define CISTPL_LONGLINK_CB	0x02	/*  CardBus PCカードのロングリンク  */
#define CISTPL_LONGLINK_MFC	0x06	/*  マルチファンクションPCカードの  */
					/*  ロングリンク  */
					
#define CISTPL_FUNCID		0x21	/*  ファンクションクラス識別  */
#define CISTPL_FUNCE		0x22	/*  ファンクション拡張  */

const static UB long_link_tuple[] = {
	CISTPL_LINTARGET,
	CISTPL_LONGLINK_A,
	CISTPL_LONGLINK_C,
	CISTPL_LONGLINK_CB,
	CISTPL_LONGLINK_MFC
};

#define TNUM_LONGLINK_TUPLE	(sizeof(long_link_tuple)/sizeof(UB))


/*  CISTPL_FUNCIDタプル  */
#define FUNCID_FUNCTION_INDEX			0x02
#define FUNCID_FUNCTION_FIXED_DISK		0x04

/*  CISTPL_FUNCEタプル  */
#define FUNCE_TUPLE_TYPE_INDEX			0x02
#define FUNCE_TUPLE_TYPE_DISK_DEVICE_INTERFACE	0x01
#define FUNCE_TUPLE_DATA_INDEX			0x03
#define FUNCE_TUPLE_DATA_PC_CARD_ATA		0x01


/*
 *  カード・コンフィギュレーション・レジスタの定義
 *　　
 *　　PCカードATAを想定してアドレスを決め打ちにしている。
 *　　これ以外のPCカードに対応するにはタプルを解析して、
 *　　レジスタ構成とそれらの番地を求める必要がある。
 */
#define CCOR	0x200	/*  Card Configuration Option Register  */
#define CCSR	0x202	/*  Card Configuration Status Register  */
#define PRR	0x204	/*  Pin Replacement Register  */
#define SCR	0x206	/*  Socket Copy Register  */

	/*
	 *  PCカードATAをI/Oマップとして、
	 *  コンフィギュレーションする際にCCORに設定する値
	 *  
	 *  　ビット7=0：リセット解除
	 *  　ビット6=*：割込みモード　レベル／エッジ
	 *  　ビット5-0：コンフィギュレーション・インデックス
	 *　　　　　　　　1：I/Oマップ（連続した16バイト）
	 */
#define CCOR_IO_MAP	0x01

	/*
	 *  SCR関連の定義
	 *  　マスタ／スレーブを設定するビットをマスクする
	 *  　　ビット6-4：copy numberビット
	 */
#define SCR_COPY_NUMBER_MASK	(BIT6 | BIT5 | BIT4)

/*
 *  カードタイプの定義
 */
#define TA_PCIC_ATA	0	/*  カードタイプATA  */

/*
 *  PCカードに関する情報のデータ構造
 *  　　メモリの使用効率を考慮して、定数部と可変部に分ける。
 *  　　　定数部：PCカード初期化ブロック構造体
 *  　　　可変部：メンバがオープン済みフラグのみなので特に構造体は定義しない
 */

/*
 *  PCカード初期化ブロック
 *  　　このブロックには定数のみ格納する。（ROMに配置される。）
 */
typedef struct pc_card_initial_block {
	ID	semid;			/*  セマフォID  */
	
					/*  時間はmsec単位  */
	RELTIM	detect_time;		/*  カード検出後待ち時間  */
	RELTIM	power_on_time;		/*  カード電源立ち上がり待ち時間  */
	RELTIM	reset_hold_time;	/*  カードリセット保持時間  */
	RELTIM	negate_reset_hold_time;	/*  カードリセット・ネゲート保持時間  */
} PCCINIB;


const static PCCINIB pccinib_table[] = {
	{
		PCIC_SEMID,
		TA_PCIC_CARD_DETECT,
		TA_PCIC_CARD_POWER_ON,
		TA_PCIC_CARD_RESET_HOLD,
		TA_PCIC_CARD_NEGATE_RESET_HOLD
	}
#if TNUM_PCCARD == 2
	,{
		PCIC_SEMID2,
		TA_PCIC_CARD_DETECT2,
		TA_PCIC_CARD_POWER_ON2,
		TA_PCIC_CARD_RESET_HOLD2,
		TA_PCIC_CARD_NEGATE_RESET_HOLD2
	}
#endif /*  TNUM_PCCARD == 2  */
};

/*
 *  オープン済みフラグ
 */
static BOOL openflag[TNUM_PCCARD];


/*
 *  マスタ／スレーブ属性の定義
 */
#define TA_PCIC_MASTER		0x00
#define TA_PCIC_SLAVE		0x10

static ER_BOOL pcic_check_card_type(ID cardid, UINT cardtype);
static ER_BOOL pcic_check_card_type_ata(ID cardid);
static UINT get_tuple(ID cardid, UINT wanted_tupe_code, UB *buff, UINT *p_index);


/*
 *  PCICの初期化ルーチン
 *　　
 *　　引数
 *　　　　VP_INT exinf：未使用
 *　　　　　　　　　　　静的API ATT_INI()との互換性のためにある
 */
void
pcic_initialize(VP_INT exinf)
{
	UINT	cardid;

	for (cardid = 0; cardid < TNUM_PCCARD; cardid++) {
		openflag[cardid] = FALSE;
	}
	hw_pcic_initialize();
}


/*
 *  マスタモードの設定
 *　　
 *　　引数
 *　　　　ID cardid：カードID
 *　　
 *　　備考
 *　　　　pcic_opn_card_ata()の中で使用されることを想定し、
 *　　　　　・排他制御もpcic_opn_card_ata()側で行っている
 *　　　　と仮定をしている。
 */
Inline void
pcic_set_master_mode(ID cardid)
{
	UB	scr;
	SIL_PRE_LOC;
	
	assert(cardid < TNUM_PCCARD);
	assert(openflag[cardid] == TRUE);
	
	SIL_LOC_INT();
	scr = hw_pcic_reb_ccr(cardid, SCR);
	scr &= ~SCR_COPY_NUMBER_MASK;
	scr |= TA_PCIC_MASTER;
	hw_pcic_wrb_ccr(cardid, SCR, scr);
	SIL_UNL_INT();
}

/*
 *  PCカードATAのコンフィギュレーション
 *　　
 *　　引数
 *　　　　ID cardid：カードID
 *　　
 *　　処理内容
 *　　　　PCカードATAをI/Oマップ（連続した1バイト）として、コンフィギュレーションする。
 */
Inline void
pcic_configurate_primary_io_map(ID cardid)
{
	SIL_PRE_LOC;
	
	assert(cardid < TNUM_PCCARD);
	SIL_LOC_INT();
	hw_pcic_wrb_ccr(cardid, CCOR, CCOR_IO_MAP);
	SIL_UNL_INT();
}

/*
 *  PCカードをATAモードでオープン
 *　　
 *　　引数
 *　　　　ID cardid：カードID
 */
ER
pcic_opn_card_ata(ID cardid)
{
	ER	ercd;
	ER_BOOL	is_card_ata;
	BOOL	is_card_inserted;
	ID	semid;
	const PCCINIB *pccinb;

	if (sns_ctx()) {		/* コンテキストのチェック */
		return(E_CTX);
	}
	if (TNUM_PCCARD <= cardid) {
		return(E_ID);		/* カード番号のチェック */
	}
	pccinb = &pccinib_table[cardid];

	semid = pccinb->semid;
	syscall(wai_sem(semid));
	if (openflag[cardid] == TRUE) {
		ercd = E_OBJ;		/* オープン済みかのチェック */
		goto sigsem_and_exit;
	}
	is_card_inserted = hw_pcic_detect_card(cardid);
	if (is_card_inserted == FALSE) {
		ercd = E_OBJ;		/* カードが挿入されているかのチェック */
		goto sigsem_and_exit;
	}
	dly_tsk(pccinb->detect_time);
	hw_pcic_porwer_on_card(cardid);
	dly_tsk(pccinb->power_on_time);
	hw_pcic_reset_card(cardid);
	dly_tsk(pccinb->reset_hold_time);
	hw_pcic_negate_reset_card(cardid);
	dly_tsk(pccinb->negate_reset_hold_time);
	hw_pcic_open_memory_window(cardid);
	hw_pcic_select_attribute_memory(cardid);

	/* PCカードATAか否かのチェック */
	is_card_ata = pcic_check_card_type(cardid, TA_PCIC_ATA);
	if ((BOOL)is_card_ata != TRUE) {
		hw_pcic_close_memory_window(cardid);
		hw_pcic_porwer_off_card(cardid);
		ercd = E_OBJ;		
		goto sigsem_and_exit;
	}
	pcic_configurate_primary_io_map(cardid);
	hw_pcic_open_io_window(cardid);
	hw_pcic_set_io_card_mode(cardid);
	openflag[cardid] = TRUE;
	
	/*
	 *　ハードディスクと異なり、PCカードATAでは、複数のカードが
	 *　同じアドレスにマッピングされるような使い方はしないため、
	 *　すべてマスタモードに設定する。 
	 */
	pcic_set_master_mode(cardid);
	ercd = E_OK;

   sigsem_and_exit:
	syscall(sig_sem(semid));
	return(ercd);
}

/*
 *  PCカードをクローズ
 *　　
 *　　引数
 *　　　　ID cardid：カードID
 */
ER
pcic_cls_card(ID cardid)
{
	ER	ercd;
	BOOL	is_card_inserted;
	ID 	semid;

	if (sns_ctx()) {		/* コンテキストのチェック */
		return(E_CTX);
	}
	if (TNUM_PCCARD <= cardid) {
		return(E_ID);		/* カード番号のチェック */
	}
	semid = pccinib_table[cardid].semid;

	syscall(wai_sem(semid));
	if (openflag[cardid] == FALSE) {
		ercd = E_OBJ;		/* オープン済みかのチェック */
		goto sigsem_and_exit;
	}
	is_card_inserted = hw_pcic_detect_card(cardid);
	if (is_card_inserted == FALSE) {
		ercd = E_OBJ;		/* カードが挿入されているかのチェック */
		goto sigsem_and_exit;
	}
	hw_pcic_close_io_window(cardid);
	hw_pcic_close_memory_window(cardid);
	hw_pcic_porwer_off_card(cardid);

	ercd = E_OK;

   sigsem_and_exit:
	syscall(sig_sem(semid));
	return(ercd);
}


/*
 *  カードタイプのチェック
 *　　
 *　　引数
 *　　　　ID cardid：カードID
 *　　　　UINT cardtype：設定モード
 *　　　　　　　cardtype = TA_PCIC_ATA：PCカードATA
 *　　　　　　　これ以外は未サポート
 *　　
 *　　返値
 *　　　　ER_BOOL ercd
 *　　　　　　　ercd < 0　　 ：エラーコード
 *　　　　　　　ercd = TRUE  ：カードタイプが引数cardtypeと一致
 *　　　　　　　ercd = FALSE ：カードタイプが引数cardtypeと不一致
 */
static ER_BOOL 
pcic_check_card_type(ID cardid, UINT cardtype)
{
	ER_BOOL ret;
	
	if (TNUM_PCCARD <= cardid) {
		return(E_ID);		/* カード番号のチェック */
	}
	
	switch(cardtype) {
		case TA_PCIC_ATA:
			ret = pcic_check_card_type_ata(cardid);
			return(ret);
			break;
		default:
			return(E_PAR);
	}				/*  PCカードATAのみサポート  */
	
}

/*
 *  カードタイプがPCカードATAか否かのチェック
 *　　
 *　　引数
 *　　　　ID cardid：カードID
 *　　
 *　　返値
 *　　　　BOOL ercd
 *　　　　　　　ercd = TRUE  ：カードタイプがPCカードATA
 *　　　　　　　ercd = FALSE ：カードタイプがPCカードATAでない
 */
static BOOL 
pcic_check_card_type_ata(ID cardid)
{
	UINT length, index;
	UB tuple[TMAX_PCIC_TULPE_LENGTH];
	
	index = 0;
	length = get_tuple(cardid, CISTPL_FUNCID, tuple, &index);
	assert(length >= FUNCID_FUNCTION_INDEX);
	if (tuple[FUNCID_FUNCTION_INDEX] != FUNCID_FUNCTION_FIXED_DISK) {
		return(FALSE);
	}
	length = get_tuple(cardid, CISTPL_FUNCE, tuple, &index);
	assert(length >= FUNCE_TUPLE_DATA_INDEX);
	if ((tuple[FUNCE_TUPLE_TYPE_INDEX] == FUNCE_TUPLE_TYPE_DISK_DEVICE_INTERFACE)
	   && (tuple[FUNCE_TUPLE_DATA_INDEX] == FUNCE_TUPLE_DATA_PC_CARD_ATA)) {
		return(TRUE);
	} else {
		return(FALSE);
	}
}

/*
 *  タプル取得
 *　　
 *　　引数
 *　　　　ID cardid：カードID
 *　　　　UINT wanted_tuple_code：取得するタプルを指定するタプルコード
 *　　　　UB *buff：タプルデータをコピーするバッファ領域の先頭番地
 *　　　　UINT *p_index：タプルの読み取り開始箇所を格納する領域の先頭番地
 *　　
 *　　返値
 *　　　　UINT length：バッファにコピーしたデータのバイト数
 */
static UINT 
get_tuple(ID cardid, UINT wanted_tuple_code, UB *buff, UINT *p_index)
{
	UINT 	i, tuple_link;
	UB	tuple_code;
	
	assert(cardid < TNUM_PCCARD);
	assert(*p_index < TSZ_PCIC_TULPE_AREA);
	
	while(*p_index < TSZ_PCIC_TULPE_AREA) {
		tuple_code = hw_pcic_reb_cis(cardid, *p_index);
		if (tuple_code == CISTPL_END) {
			return 0;	/*  連鎖終端タプルなら解析終了  */
		}
		if (tuple_code == CISTPL_NULL) {
			(*p_index)++;	/*  NULLタプルならスキップ  */
			continue;
		}
		for(i = 0; i < TNUM_LONGLINK_TUPLE; i++) {
			if (tuple_code == long_link_tuple[i]) {
				syslog(LOG_EMERG, "long link tuple is unsupported.\n");
				kernel_exit();
			}
		}
		tuple_link = hw_pcic_reb_cis(cardid, (*p_index + 1));
		tuple_link += 2;
		if (tuple_code == wanted_tuple_code) {
			/*  探しているタプルコードと一致したらバッファにコピー  */
			for (i = 0; i < tuple_link; i++) {
				buff[i] = hw_pcic_reb_cis(cardid, (*p_index + i));
			}
			(*p_index) += tuple_link;
			return tuple_link;
		} else {
			/*  タプルコードが不一致ならば、次のタプルへ  */
			(*p_index) += tuple_link;
		}
	}
	return 0;
}

