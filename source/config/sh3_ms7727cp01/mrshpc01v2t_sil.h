/*
 *  FatFs for TOPPERS
 *      FAT File system/
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 * 
 *  Copyright (C) 2005- by Industrial Technology Institute,
 *                          Miyagi Prefectural Government, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 */

/*
 *  丸文MR-SHPC-01 V2TのPDIC用のシステム・インターフェース・レイヤ（SIL）
 *  　　このファイルにはプロセッサとボードの結線に依存する処理を記述する。
 */
#ifndef _MRSHPC01V2T_SIL_H_
#define _MRSHPC01V2T_SIL_H_

#include <s_services.h>		/*  sil.h  */
#include "utils.h"
#include "mrshpc01v2t.h"

/*
 *  SH3レジスタの定義
 */

/*  バス・ステート・コントロールレジスタ1（16ビット）  */
#define SH3_BSC_BCR1		0xffffff60u
#define SH3_BSC_BCR1_A6PCM	BIT0

/*  バス・ステート・コントロールレジスタ2（16ビット）  */
#define SH3_BSC_BCR2		0xffffff62u
#define SH3_BSC_BCR2_A6SZ1	BIT13
#define SH3_BSC_BCR2_A6SZ0	BIT12

/*  ウェイト・ステート・コントロールレジスタ1（16ビット）  */
#define SH3_BSC_WCR1		0xffffff64u
#define SH3_BSC_WCR1_WAITSEL	BIT15
#define SH3_BSC_WCR1_A6IW1	BIT13
#define SH3_BSC_WCR1_A6IW0	BIT12

/*  ウェイト・ステート・コントロールレジスタ2（16ビット）  */
#define SH3_BSC_WCR2		0xffffff66u
#define SH3_BSC_WCR2_A6W2	BIT15
#define SH3_BSC_WCR2_A6W1	BIT14
#define SH3_BSC_WCR2_A6W0	BIT13
#define SH3_BSC_WCR2_A6W	(SH3_BSC_WCR2_A6W2 | SH3_BSC_WCR2_A6W1	\
				| SH3_BSC_WCR2_A6W0)

/*
 *  アドレス・マッピングの定義
 */

/*
 *  P2領域のオフセット
 *  　物理アドレス固定、キャッシュなし
 */
#define SH3_P2_AREA_OFFSET		0xa0000000u

/*
 *  PCIC内部の制御レジスタのベースアドレス
 *  　SA25-22端子の入力信号によって決まる。
 *  　注意：2バイトアクセスできる領域に割り振る
 */
#define MRSHPC01V2T_REG_BASE_ADDR	(0x183fff00u | SH3_P2_AREA_OFFSET)

/*
 *  メモリウィンドウのスタートアドレス
 *  　注意：1バイトアクセスできるようReal Byte領域に割り振る
 */
#define MRSHPC01V2T_MEM_WIN_BASE	(0x19000000u + SH3_P2_AREA_OFFSET)
#define MRSHPC01V2T_MEM_WIN_REAL_BYTE					\
		(MRSHPC01V2T_MEM_WIN_BASE + MRSHPC01V2T_REAL_BYTE_OFFSET)

/*
 *  システムアドレスのSA25-20を取り出すマクロ
 */
#define GET_SA25_20(data)	(((data) >> 20u) & 0x3fu)		

/*
 *  メモリウィンドウのスタートアドレス（システム側のSA25-20）
 */
#define MRSHPC01V2T_MEM_WIN_SA25_20	GET_SA25_20(MRSHPC01V2T_MEM_WIN_BASE)

/*
 *  I/Oウィンドウのスタートアドレス
 *  　注意：1バイトアクセスできるようReal Byte領域に割り振る
 */
#define MRSHPC01V2T_IO_WIN_BASE		(0x19100000u + SH3_P2_AREA_OFFSET)
#define MRSHPC01V2T_IO_WIN_REAL_BYTE					\
		(MRSHPC01V2T_IO_WIN_BASE + MRSHPC01V2T_REAL_BYTE_OFFSET)
#define MRSHPC01V2T_IO_WIN_REAL_WORD					\
		(MRSHPC01V2T_IO_WIN_BASE + MRSHPC01V2T_REAL_WORD_OFFSET)

/*
 *  I/Oウィンドウのスタートアドレス（システム側のSA25-20）
 */
#define MRSHPC01V2T_IO_WIN_SA25_20	GET_SA25_20(MRSHPC01V2T_IO_WIN_BASE)

/*
 *  メモリウィンドウのカード・アドレス（カード側のCA25-18）
 */
#define MRSHPC01V2T_MEM_WIN_CA25_18	0x00000000u

/*
 *  I/Oウィンドウのカード・アドレス（カード側のCA25-18）
 */
#define MRSHPC01V2T_IO_WIN_CA25_18	0x00000000u

#define TMAX_CIS_OFFSET			0x100u

/*
 *  バス、I/Oポート等の初期化
 *  　
 *  　PCICデバイスへの最初のアクセス（モード設定）を確実にするため、
 *  　最低速度でアクセスする。
 *  　必要に応じて、mrshpc01v2t_set_mwb()、mrshpc01v2t_set_iowb()で
 *  　バスの設定を変更する。
 */
Inline void
mrshpc01v2t_sil_initialize(void)
{
	VH reg;
	
	/*  エリア6を通常メモリとしてアクセス  */
	reg = sil_reh_mem((VP)SH3_BSC_BCR1);
	reg &= ~SH3_BSC_BCR1_A6PCM;
	sil_wrh_mem((VP)SH3_BSC_BCR1, reg);
	
	/*  エリア6：16ビットバス幅  */
	reg = sil_reh_mem((VP)SH3_BSC_BCR2);
	reg = (reg | SH3_BSC_BCR2_A6SZ1) & ~SH3_BSC_BCR2_A6SZ0;
	sil_wrh_mem((VP)SH3_BSC_BCR2, reg);
	
	/*  エリア6：WAIT信号イネーブル  */
	/*  　　　　 3アイドルサイクル挿入  */
	reg = sil_reh_mem((VP)SH3_BSC_WCR1);
	reg |= SH3_BSC_WCR1_WAITSEL | (SH3_BSC_WCR1_A6IW1 | SH3_BSC_WCR1_A6IW0);
	sil_wrh_mem((VP)SH3_BSC_WCR1, reg);

	/*  エリア6：挿入ウェイト 10ステート  */
	reg = sil_reh_mem((VP)SH3_BSC_WCR2);
	reg = reg | SH3_BSC_WCR2_A6W;
	sil_wrh_mem((VP)SH3_BSC_WCR2, reg);
}

/*
 *  メモリ・ウィンドウ空間のバスの設定
 */
Inline void
mrshpc01v2t_set_mwb(void)
{
}

/*
 *  I/Oウィンドウ空間のバスの設定
 */
Inline void
mrshpc01v2t_set_iowb(void)
{
}

/*
 *  PCIC内部の制御レジスタのベースアドレスへのアクセスルーチン
 *  　　自然なエンディアンで2バイト単位でアクセスする
 */

/*
 *  制御レジスタの読み出し
 */
Inline UH
mrshpc01v2t_reh_reg(UINT offset)
{
	UB *addr;
	VH data;
	
	addr = (UB*)MRSHPC01V2T_REG_BASE_ADDR + offset;
	data = sil_reh_mem((VP)addr);
	return ((UH)data);
}

/*
 *  制御レジスタへの書き込み
 */
Inline void
mrshpc01v2t_wrh_reg(UINT offset, UH data)
{
	UB *addr;
	
	addr = (UB*)MRSHPC01V2T_REG_BASE_ADDR + offset;
	sil_wrh_mem((VP)addr, (VH)data);
}

/*
 *  制御レジスタへのAND演算
 */
Inline void
mrshpc01v2t_anh_reg(UINT offset, UH data)
{
	UH reg;
	
	reg = mrshpc01v2t_reh_reg(offset);
	reg &= data;
	mrshpc01v2t_wrh_reg(offset, reg);
}

/*
 *  制御レジスタへのOR演算
 */
Inline void
mrshpc01v2t_orh_reg(UINT offset, UH data)
{
	UH reg;
	
	reg = mrshpc01v2t_reh_reg(offset);
	reg |= data;
	mrshpc01v2t_wrh_reg(offset, reg);
}

/***********************************************************
 *  以下、GDICに提供するアクセスルーチン
 ***********************************************************/

/*
 *  PCカードへのアクセスルーチン
 */

/*
 *  CIS（Card Infomation Structure）の読み出し
 *  　引数
 *  　　　ID cardid：カードID
 *  　　　UINT offset：カードの先頭番地からのオフセット（2バイト単位）
 *  　戻り値
 *  　　　UB val：読み出したCISの値
 */
Inline UB
mrshpc01v2t_reb_cis(ID cardid, UINT offset)
{
	UB *addr;
	VB data;
	
	assert(cardid == 0);			/*  カードIDのチェック  */
	assert(offset < TMAX_CIS_OFFSET);	/*  offsetの範囲チェック  */
	
	addr = (UB*)MRSHPC01V2T_MEM_WIN_REAL_BYTE + (offset * 2);
	data = sil_reb_mem((VP)addr);
	return ((UB)data);
}

/*
 *  CCR（Card Configuration Register）の読み出し
 *  　引数
 *  　　　ID cardid：カードID
 *  　　　UINT offset：カードの先頭番地からのオフセット
 *  　戻り値
 *  　　　UB val：読み出したCCRの値
 */
Inline UB
mrshpc01v2t_reb_ccr(ID cardid, UINT offset)
{
	UB *addr;
	VB data;
	
	assert(cardid == 0);		/*  カードIDのチェック  */
	assert((offset % 2) == 0);	/*  オフセットは偶数のみ  */
	
	addr = (UB*)MRSHPC01V2T_MEM_WIN_REAL_BYTE + offset;
	data = sil_reb_mem((VP)addr);
	return ((UB)data);
}

/*
 *  CCR（Card Configuration Register）への書き込み
 *  　引数
 *  　　　ID cardid：カードID
 *  　　　UINT offset：カードの先頭番地からのオフセット
 *  　　　UB data：書き込む値
 */
Inline void
mrshpc01v2t_wrb_ccr(ID cardid, UINT offset, UB data)
{
	UB *addr;
	
	assert(cardid == 0);		/*  カードIDのチェック  */
	assert((offset % 2) == 0);	/*  オフセットは偶数のみ  */
	
	addr = (UB*)MRSHPC01V2T_MEM_WIN_REAL_BYTE + offset;
	sil_wrb_mem((VP)addr, (VB)data);
}


/***********************************************************
 *  以下、ATAドライバに提供するアクセスルーチン
 ***********************************************************/

/*
 *  I/Oウィンドウのアクセスルーチン
 *  　アクセスサイズ（ワード／バイト）によって領域が異なる。
 */

#define ADD_OFFSET_READ_BYTE(offset)	\
		(VP)((UB*)(MRSHPC01V2T_IO_WIN_REAL_BYTE) + (offset))
#define ADD_OFFSET_REAL_WORD(offset)	\
		(VP)((UB*)(MRSHPC01V2T_IO_WIN_REAL_WORD) + (offset))

/*  1バイト・リード  */
Inline UB
iowin_reb_mem(UINT offset) {
	UB data;
	VP addr = ADD_OFFSET_READ_BYTE(offset);
	data = (UB)sil_reb_mem(addr);
	return data;
}

/*  1バイト・ライト  */
Inline void
iowin_wrb_mem(UINT offset, UB data) {
	VP addr = ADD_OFFSET_READ_BYTE(offset);
	sil_wrb_mem(addr, (VB)data);
}

/*  2バイト・リード  */
Inline UH
iowin_reh_lem(UINT offset) {
	UH data;
	VP addr = ADD_OFFSET_REAL_WORD(offset);
	data = (UH)sil_reh_lem(addr);
	return data;
}

/*  2バイト・ライト  */
Inline void
iowin_wrh_lem(UINT offset, UH data) {
	VP addr = ADD_OFFSET_REAL_WORD(offset);
	sil_wrh_lem(addr, (VH)data);
}


#endif /* _MRSHPC01V2T_SIL_H_ */
