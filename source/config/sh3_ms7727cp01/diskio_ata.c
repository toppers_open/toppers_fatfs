/*
 *  FatFs for TOPPERS
 *      FAT File system/
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 * 
 *  Copyright (C) 2005- by Industrial Technology Institute,
 *                          Miyagi Prefectural Government, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 */

/*
 *  FatFsが規定している下位レイヤI/F（ATAドライバとのグルーロジック）
 *  　本来は複数ドライブに対応すべきだが、このターゲットボードでは
 *  　ドライブは１つのみサポートしている。
 */

#include <string.h>	/*  memcpy()  */
#include <itron.h>
#include "pcic.h"
#include "ata.h"
#include "ata_command.h"
#include "diskio.h"

#define CARDID		0	/*  PCカードのID  */

/*
 *  物理ドライブ番号のチェックマクロ
 *  　このターゲットボードでは、ディスクは１つだけ
 */
#define CHECK_DRIVE(drv)	assert(((drv)==0u))

/*  初期化フラグ  */
static DSTATUS status = STA_NOINIT;


/*
 *  ローカル・バッファ
 *　　ATAコマンドでは2バイト単位でデータを読み書きするため、
 *　　上位層から奇数番地を与えられた場合は、一旦、ローカル
 *　　バッファにコピーする。（あまり、うまい方法ではない。）
 */
#define BUFFER_SECTOR		1u
#define SECTOR_SIZE		512u
static UH local_buffer[BUFFER_SECTOR * SECTOR_SIZE / 2u];


/*
 *  ディスク・ドライブの初期化
 *　　引数
 *　　　 BYTE Drive	物理ドライブ番号
 */
DSTATUS
disk_initialize(BYTE Drive)
{
	ER err;
	
	CHECK_DRIVE(Drive);	/*  物理ドライブ番号のチェック  */
	/*  PCカードの初期化  */
	pcic_initialize(0);
	err = pcic_opn_card_ata(CARDID);
	if (err != E_OK) {
		status = STA_NOINIT;
		return status;
	}

	/*  ATAコントローラの初期化  */
	err = ata_initialize_device();
	if (err == E_OK) {
		status = 0;
	} else {
		status = STA_NOINIT;
	}
	return status;
}


/*
 *  ディスク・ドライブの状態取得
 *　　引数
 *　　　 BYTE Drive	物理ドライブ番号
 */
DSTATUS
disk_status(BYTE Drive)
{
	CHECK_DRIVE(Drive);	/*  物理ドライブ番号のチェック  */
	return status;
}


/*
 *  ディスクからの読み込み
 *　　引数
 *　　　 BYTE Drive　　　　　物理ドライブ番号
 *　　　 BYTE* Buffer        読み出しバッファへのポインタ
 *　　　 DWORD SectorNumber  読み出し開始セクタ番号
 *　　　 BYTE SectorCount    読み出しセクタ数
 */
DRESULT
disk_read(BYTE Drive, BYTE* Buffer, DWORD SectorNumber, BYTE SectorCount)
{
	DRESULT result;
	ER err;
	
	CHECK_DRIVE(Drive);	/*  物理ドライブ番号のチェック  */
	if ((status & STA_NOINIT) != 0) {
		return RES_NOTRDY;
	}
	if ((((UINT)Buffer) & 0x1u) == 0x0u) {	/*  偶数番地  */
		err = ata_cmd_read_sector(ATA_DEVICE0, (UW)SectorNumber, SectorCount, (UH *)Buffer);
	} else {				/*  奇数番地  */
	/*
	 *　ATAコマンドでは2バイト単位でデータを読み書きするため、
	 *　上位層から奇数番地を与えられた場合は、一旦、ローカル
	 *　バッファにコピーする。（あまり、うまい方法ではない。）
	 */
		assert(SectorCount <= BUFFER_SECTOR);
		err = ata_cmd_read_sector(ATA_DEVICE0, (UW)SectorNumber, SectorCount, local_buffer);
		memcpy(Buffer, local_buffer, (SectorCount * SECTOR_SIZE));
	}
	if (err == E_OK) {
		result = RES_OK;
	} else {
		result = RES_ERROR;
	}
	return result;
}

/*
 *  ディスクへの書き込み
 *　　引数
 *　　　 BYTE Drive　　　　　物理ドライブ番号
 *　　　 BYTE* Buffer        書き込むデータへのポインタ
 *　　　 DWORD SectorNumber  書き込み開始セクタ番号
 *　　　 BYTE SectorCount    書き込みセクタ数
 */
DRESULT
disk_write(BYTE Drive, const BYTE* Buffer, DWORD SectorNumber, BYTE SectorCount)
{
	DRESULT result;
	ER err;
	
	CHECK_DRIVE(Drive);	/*  物理ドライブ番号のチェック  */
	if ((status & STA_NOINIT) != 0) {
		return RES_NOTRDY;
	}
	if ((((UINT)Buffer) & 0x1u) == 0x0u) {	/*  偶数番地  */
		err = ata_cmd_write_sector(ATA_DEVICE0, (UW)SectorNumber, SectorCount, (const UH *)Buffer);
	} else {				/*  奇数番地  */
	/*
	 *　ATAコマンドでは2バイト単位でデータを読み書きするため、
	 *　上位層から奇数番地を与えられた場合は、一旦、ローカル
	 *　バッファにコピーする。（あまり、うまい方法ではない。）
	 */
		assert(SectorCount <= BUFFER_SECTOR);
		memcpy(local_buffer, Buffer, (SectorCount * SECTOR_SIZE));
		err = ata_cmd_write_sector(ATA_DEVICE0, (UW)SectorNumber, SectorCount, local_buffer);
	}
	if (err == E_OK) {
		result = RES_OK;
	} else {
		result = RES_ERROR;
	}
	return result;
}

/*
 *  日付・時刻の取得
 *  　　ToDo：未実装
 */
DWORD
get_fattime(void)
{
	return ((DWORD)0);
}

