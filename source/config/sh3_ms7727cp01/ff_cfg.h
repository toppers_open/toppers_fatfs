/*
 *  FatFs for TOPPERS
 *      FAT File system/
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 * 
 *  Copyright (C) 2007- by Industrial Technology Institute,
 *                              Miyagi Prefectural Government, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 */

/*--------------------------------------------------------------------------/
/  FatFs - FAT file system module  R0.04                     (C)ChaN, 2007
/---------------------------------------------------------------------------/
/ FatFs module is an experimenal project to implement FAT file system to
/ cheap microcontrollers. This is a free software and is opened for education,
/ research and development under license policy of following trems.
/
/  Copyright (C) 2007, ChaN, all right reserved.
/
/ * The FatFs module is a free software and there is no warranty.
/ * You can use, modify and/or redistribute it for personal, non-profit or
/   profit use without any restriction under your responsibility.
/ * Redistributions of source code must retain the above copyright notice.
/---------------------------------------------------------------------------*/

/*
 *  FatFsのコンフィギュレーション・パラメータ
 */

#ifndef _FATFS_CFG
#define _FATFS_CFG

#define _MCU_ENDIAN		2
/*
 *  
 *  _MCU_ENDIANはFAT構造にアクセスする方法を定義する。
 *  　1: ワード・アクセス可能
 *  　2: ワード・アクセス不可能
 *  
 *  プロセッサが以下のいずれかに該当する場合は_MCU_ENDIANを2に定義する
 *  必要がある。
 *  そうでなければ、このマクロを1に定義すればよく、その場合、コードサ
 *  イズを削減できる。
 *
 *  - プロセッサのバイト・オーダーがビッグエンディアンである。
 *  - アラインに合っていないメモリ・アクセスが禁止されている。
 */
/* The _MCU_ENDIAN defines which access method is used to the FAT structure.
/  1: Enable word access.
/  2: Disable word access and use byte-by-byte access instead.
/  When the architectural byte order of the MCU is big-endian and/or address
/  miss-aligned access is prohibited, the _MCU_ENDIAN must be set to 2.
/  If it is not the case, it can be set to 1 for good code efficiency. */

#define _FS_READONLY	0
/*
 *  _FS_READONLYを1に設定するとリード・オンリー構成となる。
 *  このとき以下の書き込み機能が削除される。
 *  　・f_write, f_sync, f_unlink, f_mkdir, f_chmod, f_rename 
 *  　・不要なf_getfree
 */
/* Setting _FS_READONLY to 1 defines read only configuration. This removes
/  writing functions, f_write, f_sync, f_unlink, f_mkdir, f_chmod, f_rename
/  and useless f_getfree. */

#define _FS_MINIMIZE	0
/*
 *  _FS_MINIMIZEオプションでは、最小化レベルを定義し、以下の機能が
 *  選択される。
 *  　0:全機能が有効
 *  　1:f_stat, f_getfree, f_unlink, f_mkdir, f_chmod, f_renameを削除
 *  　2:1に加えて、f_opendir, f_readdirを削除
 */
/* The _FS_MINIMIZE option defines minimization level to remove some functions.
/  0: Full function.
/  1: f_stat, f_getfree, f_unlink, f_mkdir, f_chmod and f_rename are removed.
/  2: f_opendir and f_readdir are removed in addition to level 1. */

#define _DRIVES		1
/*  論理ドライブ数  */
/* Number of logical drives to be used */

#if 0
#define	_USE_SJIS
#endif
/*
 *  _USE_SJISが定義されると、ファイル名としてシフトJISコードが使用で
 *  きる。（未テスト）
 */
/* When _USE_SJIS is defined, Shift-JIS code transparency is enabled, otherwise
/  only US-ASCII(7bit) code can be accepted as file/directory name. */

#if 0
#define	_USE_MKFS
#endif
/*
 *  _USE_MKFSが定義され、_FS_READONLYが0のとき、f_mkfsの機能が使用でき
 *  る。（未テスト）
 */
/* When _USE_MKFS is defined and _FS_READONLY is set to 0, f_mkfs function
   is enabled. */

#endif
