/*
 *  FatFs for TOPPERS
 *      FAT File system/
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 * 
 *  Copyright (C) 2005- by Industrial Technology Institute,
 *                          Miyagi Prefectural Government, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 */

#ifndef _ATA_CFG_H_
#define _ATA_CFG_H_

/*
 *  ATAドライバ（APIレイヤ）のコンフィギュレーション・パラメータ
 */

/*  １セクタ当たりのワード数  */
#define WORD_PRE_SECTOR		256

/*
 *  デバイス数
 *  　　ToDo：デバイス数が２の場合は未テスト
 */
#define TNUM_ATA_DEVICE		1

/*
 *  デバッグ切り替え用マクロ
 *　　エラーチェックにassertマクロを使用し、
 *　　エラーの場合は、その場で停止する。
 */
#define ATA_DEBUG

/*
 *  初期化時にSET_FEATUREコマンドを発行するか否か
 *
 *  ToDo
 *　　SunDiskのCFでは動作したが、BUFFALOのCFでは動作せず
 *　　実行時に混乱を招くので、コメントアウトしている。
 */
#if 0
#define ATA_SET_FEATURE_TRANSFER_MODE
#endif

/*  シグネチャ取得の最大繰り返し回数  */
#define TMAX_RETRY_GET_SIGNATURE	10

/*  INDETIFY DEVICEコマンドを連続発行する際の時間間隔 [msec]  */
#define INTERVAL_INDETIFY_DEVICE	5

/*  INDETIFY DEVICEコマンドの最大繰り返し回数  */
#define TMAX_RETRY_IDENTIFY		10

/*  ソフトウェア・リセットの保持時間 [msec]  */
#define SOFTWARE_RESET_TIME	5

/*  ソフトウェア・リセット・ネゲートの保持時間 [msec]  */
#define SOFTWARE_RESET_NEGATE_TIME	5

/*  BSYビットクリア待ち最大繰り返し回数  */
#define TMAX_WAIT_BSY_CLR		100000

/*  BSYビット及びDRQビットクリア待ち最大繰り返し回数  */
#define TMAX_WAIT_BSY_DRQ		100000

/*  DRDYビットセット待ち待ち最大繰り返し回数  */
#define TMAX_WAIT_DRDY_SET		100000


#endif /* _ATA_CFG_H_ */
