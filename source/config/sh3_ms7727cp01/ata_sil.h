/*
 *  FatFs for TOPPERS
 *      FAT File system/
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 * 
 *  Copyright (C) 2005- by Industrial Technology Institute,
 *                          Miyagi Prefectural Government, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 */

/*
 *  ATAドライバ用システムインターフェースレイヤ（SIL）
 */

#ifndef _ATA_SIL_H_
#define _ATA_SIL_H_

#include <s_services.h>		/*  sil.h  */
#include "mrshpc01v2t_sil.h"

/*
 *  ATAレジスタのアドレス定義
 * 　コンフィギュレーション・インデックス１の場合
 * 　　I/Oモード：16バイトの連続したI/Oモード
 */

/*  コマンドブロックレジスタ  */
#define ATA_ERROR		0x01		/* (R) error register */
#define ATA_FEATURE		ATA_ERROR	/* (W) feature register */
#define ATA_SEC_CNT		0x02		/* sector count */
#define ATA_SEC_NUM		0x03		/* sector # */
#define ATA_CYL_LOW		0x04		/* cylinder# low(LSB) */
#define ATA_CYL_HIGH		0x05		/* cylinder# high(MSB) */
#define ATA_DRIVE		0x06		/* Drive/Head register */
#define ATA_CMD			0x07		/* (W) command register */
#define ATA_STATUS		ATA_CMD		/* (R) status register */
#define ATA_DATA		0x08		/* alternate data register(16bit) */

/*  コントロールブロックレジスタ  */
#define ATA_ALTSTAT		0x0e		/* (R) alternate status register */
#define ATA_DEV_CTRL		ATA_ALTSTAT	/* (W) device control register */


/*
 *  ATAレジスタアドレスの範囲チェック
 *  　便宜上、DATAレジスタは除いてある。
 */
#define is_comand_block(offset)	   ((ATA_ERROR <= (offset)) && ((offset) <= ATA_CMD))
#define is_control_block(offset)   ((offset) == ATA_ALTSTAT)
#define CHECK_REG(offset)  assert(is_comand_block(offset) || is_control_block(offset))

/*
 *  ATAレジスタ アクセスルーチン
 */

/*  1バイト・リード  */
Inline UB 
ata_reb_reg(UINT offset) {
	UB data;
	CHECK_REG(offset);
	data = iowin_reb_mem(offset);
	return data;
}

/*  1バイト・ライト  */
Inline void
ata_wrb_reg(UINT offset, UB data) {
	CHECK_REG(offset);
	iowin_wrb_mem(offset, data);
}

/*
 *　Dataレジスタ・アクセス・ルーチン
 *　　2バイトデータへのアクセスはシステム依存なので、
 *　　ここで定義する。
 */

/*
 *　Dataレジスタ・リード
 */
#define ata_read_data_reg()		iowin_reh_lem(ATA_DATA)

/*
 *　Dataレジスタ・ライト
 */
#define ata_write_data_reg(data)	iowin_wrh_lem(ATA_DATA, data)

#endif /* _ATA_SIL_H_ */
