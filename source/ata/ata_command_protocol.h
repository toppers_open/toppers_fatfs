/*
 *  FatFs for TOPPERS
 *      FAT File system/
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 * 
 *  Copyright (C) 2005- by Industrial Technology Institute,
 *                          Miyagi Prefectural Government, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 */

/*
 *  ATAコマンド・プロトコル・レイヤ
 */

#ifndef _ATA_COMMAND_PROTOCOL_H_
#define _ATA_COMMAND_PROTOCOL_H_


#include "ata_typedef.h"


/*
 *  コマンド発行に必要なデータの構造
 */
typedef struct {
	/*  引数  */
	UB feature;	/*  Featrure register  */
	UB sec_cnt;	/*  Sector Count register  */
	UB sec_num;	/*  Sector Number register  */
	UB cyl_low;	/*  Cylinder Low register  */
	UB cyl_high;	/*  Cylinder High register  */
	UB device;	/*  Device/Header register  */
	UB cmd;		/*  Command register  */
	BOOL chk_drdy;	/*  DRDYビットのチェック  */
			/*  　TRUE:チェックする  */
			/*  　FALSE:チェックしない  */
			/*  　コマンドによっては、DRDYビットのチェックが必要  */
	/*  戻り値  */
	UB error;	/*  Errorレジスタの値（コマンド実行エラーの場合）  */
} ATA_COMMAND;


extern ER ata_device_selection_protocol(ATA_DEVICE_NO device);
extern ER ata_non_data_command_protocol(ATA_DEVICE_NO device, ATA_COMMAND *cmd);
extern ER ata_pio_data_in_command_protocol(ATA_DEVICE_NO device, ATA_COMMAND *cmd, 
								INT n, UH *buff);
extern ER ata_pio_data_out_command_protocol(ATA_DEVICE_NO device, ATA_COMMAND *cmd, 
							INT n, const UH *buff);


#endif /* _ATA_COMMAND_PROTOCOL_H_ */
