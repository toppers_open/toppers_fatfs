/*
 *  FatFs for TOPPERS
 *      FAT File system/
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 * 
 *  Copyright (C) 2005- by Industrial Technology Institute,
 *                          Miyagi Prefectural Government, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 */

/*
 *  ATAコマンド発行レイヤ
 */

#include <t_services.h>		/*  ER型  */
#include "ata_command.h"
#include "ata_command_protocol.h"
#include "hw_ata.h"		/*  CHECK_DEVICE_NO()  */

#define NA		0		/*  not applicable  */
#define ATA_1SECTOR	1		/*  １セクタ  */

/* ATA commands（実装していないコマンドを含む） */
#define ATA_NOP				0x00	/* NOP command */
#define		ATA_NF_FLUSHQUEUE	0x00	/* flush queued cmd's */
#define		ATA_NF_AUTOPOLL		0x01	/* start autopoll function */
#define ATA_ATAPI_RESET			0x08	/* reset ATAPI device */
#define ATA_READ			0x20	/* read command */
	/*
	 *  以前は
	 *　　0x20：リトライあり
	 *　　0x21：リトライなし
	 *  の2種類があったが、現在では0x20のみが使用されている。
	 */


#define ATA_READ48			0x24	/* read command */
#define ATA_READ_DMA48			0x25	/* read w/DMA command */
#define ATA_READ_DMA_QUEUED48		0x26	/* read w/DMA QUEUED command */
#define ATA_READ_MUL48			0x29	/* read multi command */
#define ATA_WRITE			0x30	/* write command */
#define ATA_WRITE48			0x34	/* write command */
#define ATA_WRITE_DMA48			0x35	/* write w/DMA command */
#define ATA_WRITE_DMA_QUEUED48		0x36	/* write w/DMA QUEUED command */
#define ATA_WRITE_MUL48			0x39	/* write multi command */
#define ATA_READ_FPDMA_QUEUED		0x60	/* read w/DMA NCQ */
#define ATA_WRITE_FPDMA_QUEUED		0x61	/* write w/DMA NCQ */
#define ATA_INIT_DEV_PARMS		0x91	/* initial device parameters */
#define ATA_PACKET_CMD			0xa0	/* packet command */
#define ATA_ATAPI_IDENTIFY		0xa1	/* get ATAPI params*/
#define ATA_SERVICE			0xa2	/* service command */
#define ATA_READ_MUL			0xc4	/* read multi command */
#define ATA_WRITE_MUL			0xc5	/* write multi command */
#define ATA_SET_MULTI			0xc6	/* set multi size command */
#define ATA_READ_DMA_QUEUED		0xc7	/* read w/DMA QUEUED command */
#define ATA_READ_DMA			0xc8	/* read w/DMA command */
#define ATA_WRITE_DMA			0xca	/* write w/DMA command */
#define ATA_WRITE_DMA_QUEUED		0xcc	/* write w/DMA QUEUED command */
#define ATA_SLEEP			0xe6	/* sleep command */
#define ATA_FLUSHCACHE			0xe7	/* flush cache to disk */
#define ATA_FLUSHCACHE48		0xea	/* flush cache to disk */
#define ATA_ATA_IDENTIFY		0xec	/* get ATA params */
#define ATA_SETFEATURES			0xef	/* features command */
#define		ATA_SF_SETXFER		0x03	/* set transfer mode */
#define		ATA_SF_ENAB_WCACHE	0x02	/* enable write cache */
#define		ATA_SF_DIS_WCACHE	0x82	/* disable write cache */
#define		ATA_SF_ENAB_RCACHE	0xaa	/* enable readahead cache */
#define		ATA_SF_DIS_RCACHE	0x55	/* disable readahead cache */
#define		ATA_SF_ENAB_RELIRQ	0x5d	/* enable release interrupt */
#define		ATA_SF_DIS_RELIRQ	0xdd	/* disable release interrupt */
#define		ATA_SF_ENAB_SRVIRQ	0x5e	/* enable service interrupt */
#define		ATA_SF_DIS_SRVIRQ	0xde	/* disable service interrupt */

/*
 *  SET FEATURESコマンド
 *　　Subcommand code：Set transfer mode
 */
#define SUBCOMAND_SET_TRANSFER_MODE	0x03
#define MAX_PIO_MODE_NO			4	/*  PIO転送モード番号の最大値  */

/*
 *  LBA(logical block address)の操作
 */
#define LBA7_0(lba)	LO8(lba)
#define LBA15_8(lba)	HI8(lba)
#define LBA23_16(lba)	(UB)(((lba) >> 16) & 0xff)
#define LBA27_24(lba)	(UB)(((lba) >> 24) & 0xf)

/*
 *  INDETIFY DEVICEコマンド
 *
 *　　引数：
 *　　　　ATA_DEVICE_NO device：デバイス番号
 *　　　　UH *identify_data：読み出した情報を格納する領域の先頭アドレス
 *　　戻り値：
 *　　　　E_OK			正常終了
 *　　　　E_TMOUT		タイムアウト
 *　　　　E_EXE			コマンド実行エラー
 */
ER 
ata_cmd_identify_device(ATA_DEVICE_NO device, UH *identify_data) {
	ATA_COMMAND cmd;
	ER err;
	
	CHECK_DEVICE_NO(device);
	cmd.feature = NA;
	cmd.sec_cnt = NA;
	cmd.sec_num = NA;
	cmd.cyl_low = NA;
	cmd.cyl_high = NA;
	cmd.device = 0;		/*  DEVビットはプロトコル側で設定する  */
	cmd.cmd = ATA_ATA_IDENTIFY;
	cmd.chk_drdy = TRUE;	/*  DRDYビットをチェックする   */
	
	err = ata_pio_data_in_command_protocol(device, &cmd, 
						ATA_1SECTOR, identify_data);
	return err;
}

#ifdef ATA_SET_FEATURE_TRANSFER_MODE
/*
 *  SET FEATURESコマンド
 *　　Subcommand code：Set transfer mode
 *　　
 *　　引数：
 *　　　　ATA_DEVICE_NO device：デバイス番号
 *　　　　UB trnasfer_mode：転送モード
 *　　　　	PIO_DEFAULT：PIO転送デフォルトモード
 *　　　　	PIO_DEFAULT_NO_IORDY：PIO転送デフォルトモード(IORDY無効）
 *　　　　	PIO_FLOW_CTRL：PIOフローコントロール転送モード
 *　　　　	
 *　　　　UB trnasfer_mode_no：転送モード番号0〜3
 *　　戻り値：
 *　　　　E_OK			正常終了
 *　　　　E_TMOUT		タイムアウト
 *　　　　E_EXE			コマンド実行エラー
 *　　　　	
 * ToDo
 *　　SunDiskのCFでは動作したが、BUFFALOのCFでは動作せず
 *　　実行時に混乱を招くので、コメントアウトしている。
 */
ER 
ata_cmd_set_features_transfer_mode(ATA_DEVICE_NO device, UB trnasfer_mode, UB trnasfer_mode_no) {
	ATA_COMMAND cmd;
	ER err;
	
	CHECK_DEVICE_NO(device);
	assert((trnasfer_mode_no & ~0x03U) == 0U);	/*  2ビット以内  */
	cmd.feature = SUBCOMAND_SET_TRANSFER_MODE;
	cmd.sec_num = NA;
	cmd.cyl_low = NA;
	cmd.cyl_high = NA;
	cmd.device = NA;	/*  DEVビットはプロトコル側で設定する  */
	cmd.cmd = ATA_SETFEATURES;

	switch(trnasfer_mode) {
		case PIO_DEFAULT:	   /*  PIO転送デフォルトモード  */
		case PIO_DEFAULT_NO_IORDY: /*  PIO転送デフォルトモード(IORDY無効）*/
					   /*  （PIO転送モード3以上で利用可能）  */
			cmd.sec_cnt = trnasfer_mode;
			break;
		case PIO_FLOW_CTRL:	   /*  PIOフローコントロール転送モード  */
			cmd.sec_cnt = trnasfer_mode | trnasfer_mode_no;
			break;
		default:
			assert("Invalid transfer mode");
	}

	cmd.chk_drdy = TRUE;	/*  DRDYビットをチェックする   */
	
	err = ata_non_data_command_protocol(device, &cmd);
	
	return err;
}
#endif	 /* ATA_SET_FEATURE_TRANSFER_MODE */

/*
 *  INITIALIZE DEVICE PARAMETERSコマンド
 *
 *　　引数：
 *　　　　ATA_DEVICE_NO device：デバイス番号
 *　　　　UB sector：論理トラックあたりの論理セクタ数
 *　　　　UB head：最大ヘッド数
 *　　戻り値：
 *　　　　E_OK			正常終了
 *　　　　E_TMOUT		タイムアウト
 *　　　　E_EXE			コマンド実行エラー
 */
ER 
ata_cmd_initialize_device_params(ATA_DEVICE_NO device, UB sector, UB head) {
	ATA_COMMAND cmd;
	ER err;
	
	CHECK_DEVICE_NO(device);
	assert(head <= 0xfU);	/*  4ビット以内  */
	cmd.feature = NA;
	cmd.sec_cnt = sector;
	cmd.sec_num = NA;
	cmd.cyl_low = NA;
	cmd.cyl_high = NA;
	cmd.device = head;	/*  DEVビットはプロトコル側で設定する  */
	cmd.cmd = ATA_INIT_DEV_PARMS;
	cmd.chk_drdy = FALSE;	/*  DRDYビットをチェックしない   */
	
	err = ata_non_data_command_protocol(device, &cmd);
	
	return err;
}

/*
 *  READ SECTOR(S)コマンド
 *
 *　　引数：
 *　　　　ATA_DEVICE_NO device：デバイス番号
 *　　　　UW lba：LBA (logical block address)
 *　　　　UB sector：読み出すセクタ数
 *　　　　UH *buff：読み出したデータを格納する領域の先頭アドレス
 *　　戻り値：
 *　　　　E_OK			正常終了
 *　　　　E_TMOUT		タイムアウト
 *　　　　E_EXE			コマンド実行エラー
 */
ER 
ata_cmd_read_sector(ATA_DEVICE_NO device, UW lba, UB sector, UH *buff) {
	ATA_COMMAND cmd;
	ER err;
	
	CHECK_DEVICE_NO(device);
	assert((lba & 0xf0000000UL) == 0);	/*  ビット31〜28はゼロ  */
	assert((((UINT)buff) & 0x1U) == 0);	/*  偶数番地  */
	
	cmd.feature = NA;
	cmd.sec_cnt = sector;
	cmd.sec_num = LBA7_0(lba);
	cmd.cyl_low = LBA15_8(lba);
	cmd.cyl_high = LBA23_16(lba);
	cmd.device = LBA27_24(lba) | ATA_D_LBA;
				/*  DEVビットはプロトコル側で設定する  */
	cmd.cmd = ATA_READ;
	cmd.chk_drdy = TRUE;	/*  DRDYビットをチェックする   */
	
	err = ata_pio_data_in_command_protocol(device, &cmd, sector, buff);
	return err;
}

/*
 *  WIRTE SECTOR(S)コマンド
 *
 *　　引数：
 *　　　　ATA_DEVICE_NO device：デバイス番号
 *　　　　UW lba：LBA (logical block address)
 *　　　　UB sector：書き込むセクタ数
 *　　　　const UH *buff：書き込むデータの先頭アドレス
 *　　戻り値：
 *　　　　E_OK			正常終了
 *　　　　E_TMOUT		タイムアウト
 *　　　　E_EXE			コマンド実行エラー
 */
ER 
ata_cmd_write_sector(ATA_DEVICE_NO device, UW lba, UB sector, const UH *buff) {
	ATA_COMMAND cmd;
	ER err;
	
	CHECK_DEVICE_NO(device);
	assert((lba & 0xf0000000UL) == 0U);	/*  ビット31〜28はゼロ  */
	assert((((UINT)buff) & 0x1U) == 0U);	/*  偶数番地  */
	
	cmd.feature = NA;
	cmd.sec_cnt = sector;
	cmd.sec_num = LBA7_0(lba);
	cmd.cyl_low = LBA15_8(lba);
	cmd.cyl_high = LBA23_16(lba);
	cmd.device = LBA27_24(lba) | ATA_D_LBA;
				/*  DEVビットはプロトコル側で設定する  */
	cmd.cmd = ATA_WRITE;
	cmd.chk_drdy = TRUE;	/*  DRDYビットをチェックする   */
	
	err = ata_pio_data_out_command_protocol(device, &cmd, sector, buff);
	return err;
}
