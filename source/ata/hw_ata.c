/*
 *  FatFs for TOPPERS
 *      FAT File system/
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 * 
 *  Copyright (C) 2005- by Industrial Technology Institute,
 *                          Miyagi Prefectural Government, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 */

/*
 *  ATAドライバ（PDIC相当）
 */
#include "hw_ata.h"
#include "ata_cfg.h"

/*  デバイス選択  */
void 
ata_select_device(ATA_DEVICE_NO device) {
	UB dev_head = TO_DEV_BIT(device) | ATA_D_OBS;
	ata_wrb_reg(ATA_DRIVE, dev_head);
	sil_dly_nse(ATA_400NSEC);
}

/*
 *　コマンド実行
 *　　Commandレジスタへコマンドコード書き込み
 *
 *　　引数：
 *　　　　UB code：コマンド・コード
 */
void
ata_execute_command(UB code) {
	ata_wrb_reg(ATA_CMD, code);
	sil_dly_nse(ATA_400NSEC);
}

/*
 *　BSYビットクリア待ち
 *
 *　　引数：
 *　　　　なし
 *　　戻り値：
 *　　　　E_OK			正常終了
 *　　　　E_TMOUT		タイムアウト
 */
ER 
ata_wait_bsy_clr(void) {
	int i;
	UB status;
	
	for(i = 0; i < TMAX_WAIT_BSY_CLR; i++) {
		status = ata_read_status();
		status &= ATA_S_BSY;
		if (status == 0) {
			return E_OK;
		}
	}
	return E_TMOUT;
}

/*
 *　BSYビット及びDRQビットクリア待ち
 *
 *　　引数：
 *　　　　なし
 *　　戻り値：
 *　　　　E_OK			正常終了
 *　　　　E_TMOUT		タイムアウト
 */
ER 
ata_wait_bsy_drq_clr(void) {
	int i;
	UB status;
	
	for(i = 0; i < TMAX_WAIT_BSY_DRQ; i++) {
		status = ata_read_status();
		status &= (ATA_S_BSY | ATA_S_DRQ);
		if (status == 0) {
			return E_OK;
		}
	}
	return E_TMOUT;
}

/*
 *　BSYビットクリア及びDRQビットセット待ち
 *
 *　　引数：
 *　　　　なし
 *　　戻り値：
 *　　　　E_OK			正常終了
 *　　　　E_TMOUT		タイムアウト
 */
ER 
ata_wait_bsy0_drq1(void) {
	int i;
	UB status, bsy, drq;
	
	for(i = 0; i < TMAX_WAIT_BSY_DRQ; i++) {
		status = ata_read_status();
		bsy = status & ATA_S_BSY;
		drq = status & ATA_S_DRQ;
		if ((bsy == 0) && (drq != 0)) {
			return E_OK;
		}
	}
	return E_TMOUT;
}

/*
 *　DRDYビットセット待ち
 *
 *　　引数：
 *　　　　なし
 *　　戻り値：
 *　　　　E_OK			正常終了
 *　　　　E_TMOUT		タイムアウト
 *　　備考：
 *　　　　BSYビットが0でないとDRDYビットの値は意味がない
 */
ER 
ata_wait_drdy_set(void) {
	int i;
	UB status, bsy, drdy;
	
	for(i = 0; i < TMAX_WAIT_DRDY_SET; i++) {
		status = ata_read_status();
		bsy = status & ATA_S_BSY;
		drdy = status & ATA_S_DRDY;
		if ((bsy == 0) && (drdy != 0)) {
			return E_OK;
		}
	}
	return E_TMOUT;
}


