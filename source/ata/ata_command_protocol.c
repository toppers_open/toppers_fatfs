/*
 *  FatFs for TOPPERS
 *      FAT File system/
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 * 
 *  Copyright (C) 2005- by Industrial Technology Institute,
 *                          Miyagi Prefectural Government, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 */

/*
 *  ATAコマンド・プロトコル・レイヤ
 */
#include <t_services.h>

#include "ata_cfg.h"
#include "ata_command_protocol.h"
#include "hw_ata.h"
#include "ata.h"	/*  E_EXE  */

/*
 *　Device selectionプロトコル
 *
 *　　引数：
 *　　　　ATA_DEVICE_NO device：デバイス番号
 *　　戻り値：
 *　　　　E_OK			正常終了
 *　　　　E_TMOUT		タイムアウト
 */
ER
ata_device_selection_protocol(ATA_DEVICE_NO device) {
	/*
	 *  バスがアイドル状態かチェックする
	 */
	ER err = ata_wait_bsy_drq_clr();
	if (err != E_OK) {
		return err;
	}

	/*
	 *  選択したデバイスがアイドル状態かチェックする
	 */
	ata_select_device(device);
	err = ata_wait_bsy_drq_clr();
	return err;
}

/*
 *　各ATAレジスタにコマンド・パラメータを設定する。
 *
 *　　引数：
 *　　　　ATA_DEVICE_NO device：デバイス番号
 *　　　　ATA_COMMAND *cmd：コマンド・パラメータの先頭アドレス
 *　　戻り値：
 *　　　　なし
 */
Inline void
ata_set_register(ATA_DEVICE_NO device, ATA_COMMAND *cmd) {
	UB dev_reg;
	
	ata_wrb_reg(ATA_FEATURE, cmd->feature);
	ata_wrb_reg(ATA_SEC_CNT, cmd->sec_cnt);
	ata_wrb_reg(ATA_SEC_NUM, cmd->sec_num);
	ata_wrb_reg(ATA_CYL_LOW, cmd->cyl_low);
	ata_wrb_reg(ATA_CYL_HIGH, cmd->cyl_high);
	
	dev_reg = cmd->device | TO_DEV_BIT(device) | ATA_D_OBS;
	ata_wrb_reg(ATA_DRIVE, dev_reg);
}

/*
 *　Non data commandプロトコル
 *
 *　　引数：
 *　　　　ATA_DEVICE_NO device：デバイス番号
 *　　　　ATA_COMMAND *cmd：コマンド・パラメータの先頭アドレス
 *　　戻り値：
 *　　　　E_OK			正常終了
 *　　　　E_TMOUT		タイムアウト
 *　　　　E_EXE			コマンド実行エラー
 */
ER
ata_non_data_command_protocol(ATA_DEVICE_NO device, ATA_COMMAND *cmd) {
	UB status;
	/*
	 *  Device selection protocolを実行する
	 */
	ER err = ata_device_selection_protocol(device);
	CHECK_ERR(err);
	if (err != E_OK) {
		return err;
	}
	
	/*  各ATAレジスタにコマンド・パラメータを設定  */
	ata_set_register(device, cmd);
	
	/*
	 *  DRDYビットのチェック  
	 *　　コマンドによってはDRDYビットがチェックされていないと実行できない
	 */
	if (cmd->chk_drdy) {
		err = ata_wait_drdy_set();
		CHECK_ERR(err);
		if (err != E_OK) {
			return err;
		}
	}
	
	/*  コマンド実行  */
	ata_execute_command(cmd->cmd);
	
	err = ata_wait_bsy_clr();
	CHECK_ERR(err);
	if (err != E_OK) {
		return err;
	}
	
	/*
	 *　ERRビットのチェック
	 *　　ここでStatusレジスタを読み出すのは
	 *　　割込み要求のクリアも兼ねている。
	 */
	status = ata_read_status() & ATA_S_ERR;
	if (status != 0) {
		cmd->error = ata_read_error();
		err = E_EXE;	/*  コマンド実行エラー  */
		CHECK_ERR(err);
	} else {
		err = E_OK;
	}
	CHECK_ERR(err);
	return err;
}


/*
 *　PIO data in commandプロトコル
 *
 *　　引数：
 *　　　　ATA_DEVICE_NO device：デバイス番号
 *　　　　ATA_COMMAND *cmd：コマンド・パラメータの先頭アドレス
 *　　　　INT n:読み出すセクタ数
 *　　　　UH *buff：読み出したデータを格納する領域の先頭アドレス
 *　　戻り値：
 *　　　　E_OK			正常終了
 *　　　　E_TMOUT		タイムアウト
 *　　　　E_EXE			コマンド実行エラー
 */
ER
ata_pio_data_in_command_protocol(ATA_DEVICE_NO device, ATA_COMMAND *cmd, INT n, UH *buff) {
	UB status, bsy, drq;
	INT block, i;
	
	/*
	 *  Device selection protocolを実行する
	 */
	ER err = ata_device_selection_protocol(device);
	CHECK_ERR(err);
	if (err != E_OK) {
		return err;
	}
	
	/*  各ATAレジスタにコマンド・パラメータを設定  */
	ata_set_register(device, cmd);
	
	/*
	 *  DRDYビットのチェック  
	 *　　コマンドによってはDRDYビットがチェックされていないと実行できない
	 */
	if (cmd->chk_drdy) {
		err = ata_wait_drdy_set();
		CHECK_ERR(err);
		if (err != E_OK) {
			return err;
		}
	}
	
	/*  コマンド実行  */
	ata_execute_command(cmd->cmd);
	
	/*
	 *　nブロック分のデータ読み出し
	 */
	for(block = 0; block < n; block++) {
		/*
		 *　Alternate Statusレジスタの空読み
		 *　　Statusレジスタが有効になる前に
		 *　　ポーリングを行うのを防ぐ。
		 */
		ata_read_alternate_status();
		err = ata_wait_bsy_clr();
		CHECK_ERR(err);
		if (err != E_OK) {
			return err;
		}

		/*
		 *　ERRビットのチェック
		 *　　ここでStatusレジスタを読み出すのは
		 *　　割込み要求のクリアも兼ねている。
		 */
		status = ata_read_status() & ATA_S_ERR;
		if (status != 0) {
			cmd->error = ata_read_error();
			CHECK_ERR(E_EXE);
			return E_EXE;	/*  コマンド実行エラー  */
		}
		
		/*  BSY=0, DRQ=1 待ち  */
		err = ata_wait_bsy0_drq1();
		CHECK_ERR(err);
		if (err != E_OK) {
			return err;
		}
		
		/*
		 *　１ブロック分のデータ読み出し
		 */
		for(i = 0; i < WORD_PRE_SECTOR; i++) {
			*buff = ata_read_data_reg();
			buff++;
		}
		
		/*　Alternate Statusレジスタの空読み（同上）  */
		ata_read_alternate_status();
		status = ata_read_status();
		bsy = status & ATA_S_BSY;
		drq = status & ATA_S_DRQ;
		assert(((bsy == 0) && (drq == 0)) /*  全データ転送完了  */\
			|| ((bsy != 0) && (drq == 0)) /*  次のブロックを読み出し中  */\
			|| ((bsy == 0) && (drq != 0))); /*  次のブロック読み出し完了  */
	}
	
	/*
	 *　ERRビットのチェック
	 */
	status = ata_read_status() & ATA_S_ERR;
	if (status != 0) {
		cmd->error = ata_read_error();
		err = E_EXE;	/*  コマンド実行エラー  */
	} else {
		err = E_OK;
	}
	CHECK_ERR(err);
	return err;
}

/*
 *　PIO data out commandプロトコル
 *
 *　　引数：
 *　　　　ATA_DEVICE_NO device：デバイス番号
 *　　　　ATA_COMMAND *cmd：コマンド・パラメータの先頭アドレス
 *　　　　INT n:読み出すセクタ数
 *　　　　const UH *buff：出力するデータの先頭アドレス
 *　　戻り値：
 *　　　　E_OK			正常終了
 *　　　　E_TMOUT		タイムアウト
 *　　　　E_EXE			コマンド実行エラー
 */
ER
ata_pio_data_out_command_protocol(ATA_DEVICE_NO device, ATA_COMMAND *cmd, INT n, const UH *buff) {
	UB status, bsy, drq;
	INT block, i;
	
	/*
	 *  Device selection protocolを実行する
	 */
	ER err = ata_device_selection_protocol(device);
	if (err != E_OK) {
		return err;
	}
	
	/*  各ATAレジスタにコマンド・パラメータを設定  */
	ata_set_register(device, cmd);
	
	/*
	 *  DRDYビットのチェック  
	 *　　コマンドによってはDRDYビットがチェックされていないと実行できない
	 */
	if (cmd->chk_drdy) {
		err = ata_wait_drdy_set();
		if (err != E_OK) {
			return err;
		}
	}
	
	/*  コマンド実行  */
	ata_execute_command(cmd->cmd);
	
	/*
	 *　nブロック分のデータ書き出し
	 */
	for(block = 0; block < n; block++) {
		/*
		 *　Alternate Statusレジスタの空読み
		 *　　Statusレジスタが有効になる前に
		 *　　ポーリングを行うのを防ぐ。
		 */
		ata_read_alternate_status();
		err = ata_wait_bsy_clr();
		if (err != E_OK) {
			return err;
		}

		/*
		 *　ERRビットのチェック
		 *　　ここでStatusレジスタを読み出すのは
		 *　　割込み要求のクリアも兼ねている。
		 */
		status = ata_read_status() & ATA_S_ERR;
		if (status != 0) {
			cmd->error = ata_read_error();
			return E_EXE;	/*  コマンド実行エラー  */
		}
		
		/*  BSY=0, DRQ=1 待ち  */
		err = ata_wait_bsy0_drq1();
		if (err != E_OK) {
			return err;
		}
		
		/*
		 *　１ブロック分のデータ書き出し
		 */
		for(i = 0; i < WORD_PRE_SECTOR; i++) {
			ata_write_data_reg(*buff);
			buff++;
		}
		
		/*　Alternate Statusレジスタの空読み（同上）  */
		ata_read_alternate_status();
		status = ata_read_status();
		bsy = status & ATA_S_BSY;
		drq = status & ATA_S_DRQ;
		assert(((bsy == 0) && (drq == 0)) /*  全データ転送完了  */\
			|| ((bsy != 0) && (drq == 0)) /*  次のブロックを読み出し中  */\
			|| ((bsy == 0) && (drq != 0))); /*  次のブロック読み出し完了  */
	}
	
	/*　Alternate Statusレジスタの空読み（同上）  */
	ata_read_alternate_status();

	/*
	 *　ERRビットのチェック
	 */
	status = ata_read_status() & ATA_S_ERR;
	if (status != 0) {
		cmd->error = ata_read_error();
		err = E_EXE;	/*  コマンド実行エラー  */
	} else {
		err = E_OK;
	}
	return err;
}

