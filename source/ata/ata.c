/*
 *  FatFs for TOPPERS
 *      FAT File system/
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 * 
 *  Copyright (C) 2005- by Industrial Technology Institute,
 *                          Miyagi Prefectural Government, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 */

/*
 *  ATAドライバ（APIレイヤ）
 *
 *　　・ATAPIデバイスはサポートしていない。
 *　　・以下のハードウェア構成を想定している。
 *　　　　・ATAコントローラ：１つ
 *　　　　・ATAデバイス：１〜２つ
 *　　　　　　ATAデバイスを２台接続しての動作テストはしていない。
 */

#include <t_services.h>

#include "ata_cfg.h"
#include "ata.h"
#include "ata_command.h"
#include "hw_ata.h"

/*  シグネチャのデータ構造  */
typedef struct {
	UB high;
	UB low;
} ATA_SIGNATURE;

/*  デバイスタイプ  */
typedef enum {
	ATA_DEVICE,	/*  ATAデバイス  */
	ATA_UNKNOWN,	/*  不明デバイス  */
	ATA_NO_CONNECT	/*  デバイス未接続  */
} ATA_DEVICE_TYPE;

/*  INDETIFY DEVICEコマンドのバッファ  */
#define ATA_INDETIFY_BUFF_SIZE	256	/*  ワード単位  */
static UH ata_identify_buf[TNUM_ATA_DEVICE][ATA_INDETIFY_BUFF_SIZE];
						/*  2バイト単位  */

/*  現在、アクティブなデバイス  */
static ATA_DEVICE_TYPE active_device;

/*  デバイスタイプ  */
static ATA_DEVICE_TYPE device_type[TNUM_ATA_DEVICE];

#define DUMMY_SIGNATURE		0xff	/*  シグネチャのダミーデータ  */

/*  シグネチャ取得時のエラー判定のマスクデータ  */
#define ERROR_MASK		0x7f	

/*
 *  INDETIFY DEVICEコマンドで得られたデータの解析用マクロ
 *
 *　　マジックナンバーもあるが残しておく。
 */

/*  論理ヘッダー数  */
#define get_number_of_logical_heads(table)	(((table)[3]) - 1)

/*  論理トラック当たりの論理セクタ数  */
#define get_number_of_logical_sectors_per_logical_track(table)	((table)[6])

#define PIO_MODE3	0x1
#define PIO_MODE4	0x2

#ifdef ATA_SET_FEATURE_TRANSFER_MODE
static UB get_pio_transfer_mode(UH *table);
#endif	/* ATA_SET_FEATURE_TRANSFER_MODE */

static ER ata_check_connected_device(void);

static ER
ata_get_signature(ATA_DEVICE_NO device, ATA_SIGNATURE *p_signature);

static ATA_DEVICE_TYPE
ata_get_device_type(ATA_DEVICE_NO device, ATA_SIGNATURE *p_signature, UH *buff);


/*  ATAデバイスの初期化  */
ER ata_initialize_device(void) {
	ER err;
	UB sector, head;
	
	err = ata_check_connected_device();
	if (err != E_OK) {
		return err;
	}
	
	err = ata_cmd_identify_device(ATA_DEVICE0, ata_identify_buf[ATA_DEVICE0]); 
	if (err != E_OK) {
		return err;
	}

	/*  INITIALIZE DEVICE PARAMSコマンド  */
	sector = get_number_of_logical_sectors_per_logical_track(ata_identify_buf[ATA_DEVICE0]);
	head = get_number_of_logical_heads(ata_identify_buf[ATA_DEVICE0]);
	err = ata_cmd_initialize_device_params(ATA_DEVICE0, sector, head);
	if (err != E_OK) {
		return err;
	}
	
	/*
	 * SET FEATUREコマンド（set transfer mode）
	 * 
	 * ToDo
	 *　　本来は以下のように動作モードを設定すべき。
	 *　　SunDiskのCFでは動作したが、BUFFALOのCFでは動作せず
	 *　　実行時に混乱を招くので、コメントアウトしている。
	 */
#ifdef ATA_SET_FEATURE_TRANSFER_MODE
	{
		UB mode_no;
		mode_no = get_pio_transfer_mode(ata_identify_buf[ATA_DEVICE0]);
			/*  モード番号3以上はコマンドに渡せない  */
		if (mode_no > 3) {
			mode_no = 3;
		}
		err = ata_cmd_set_features_transfer_mode(ATA_DEVICE0, 
						PIO_DEFAULT, mode_no);
	}

#endif	/*  ATA_SET_FEATURE_TRANSFER_MODE  */

	return err;
}

#ifdef ATA_SET_FEATURE_TRANSFER_MODE
/*
 *　INDETIFY DEVICEコマンドで得られたデータからPIO転送モード番号を求める
 *
 *　　引数：なし
 *　　　　UH *table：INDETIFY DEVICEコマンドで得られたデータへの先頭アドレス
 *　　戻り値：
 *　　　　UB mode：PIO転送モード番号
 *　　備考：
 *　　　　モード5以降の判別は未実装
 */
static UB
get_pio_transfer_mode(UH *table) {
	UH is_advanced_mode_supported, advanced_PIO_mode;
	UB mode = 0;	/*  ワード51は廃止  */
	
	/*
	 *　ワード64〜70（転送モードに関する記述）が
	 *　有効か否かの判定
	 */
	is_advanced_mode_supported = table[53] & BIT1;
	if (is_advanced_mode_supported) {
		advanced_PIO_mode = table[64];
		if (advanced_PIO_mode & PIO_MODE3) {
			mode = 3;
		}
		if (advanced_PIO_mode & PIO_MODE4) {
			mode = 4;
		}
	}
	return mode;
}
#endif	/* ATA_SET_FEATURE_TRANSFER_MODE */

/*
 *　ATAデバイス接続確認
 *
 *　　引数：なし
 *　　戻り値：
 *　　　　E_OK：正常終了
 *　　　　E_OBJ：有効なデバイスなし
 */
static ER 
ata_check_connected_device(void)
{
	ATA_SIGNATURE signature[TNUM_ATA_DEVICE];	/*  シグネチャ  */
	ER err0;
#if TNUM_ATA_DEVICE > 1
	ER err1;
#endif /* TNUM_ATA_DEVICE > 1 */
	
	/*  ソフトウェア・リセット（かつ、割込み禁止）  */
	ata_software_reset();
	dly_tsk(SOFTWARE_RESET_TIME);
	/*  ソフトウェア・リセット解除（かつ、割込み禁止）  */
	ata_software_reset_negate();
	dly_tsk(SOFTWARE_RESET_NEGATE_TIME);

	device_type[ATA_DEVICE0] = ATA_UNKNOWN;
#if TNUM_ATA_DEVICE > 1
	device_type[ATA_DEVICE1] = ATA_UNKNOWN;
#endif /* TNUM_ATA_DEVICE > 1 */

	/*  シグネチャ取得  */
	err0 = ata_get_signature(ATA_DEVICE0, &signature[ATA_DEVICE0]);
	CHECK_ERR(err0);
#if TNUM_ATA_DEVICE > 1
	err1 = ata_get_signature(ATA_DEVICE1, &signature[ATA_DEVICE1]);
#endif /* TNUM_ATA_DEVICE > 1 */

	/*    
	 *　デバイス・タイプのチェック
	 *　　デバイス0でDEVICE IDENTIFYコマンドを実行すると
	 *　　デバイス1のシグネチャが破壊されてしまう。
	 *　　そのため、同じような処理が2回ずつ現れるが、
	 *　　下記の処理と上記の処理をループで一括には処理できない。
	 */
	if (err0 == E_OK) {
		device_type[ATA_DEVICE0] = 
			ata_get_device_type(ATA_DEVICE0, &signature[ATA_DEVICE0],
						ata_identify_buf[ATA_DEVICE0]);
	}
#if TNUM_ATA_DEVICE > 1
	if (err1 == E_OK) {
		device_type[ATA_DEVICE1] = 
			ata_get_device_type(ATA_DEVICE1, &signature[ATA_DEVICE1],
						ata_identify_buf[ATA_DEVICE1]);
	}
#endif /* TNUM_ATA_DEVICE > 1 */
	
	if (device_type[ATA_DEVICE0] == ATA_DEVICE) {
		/*  デバイス0にATAデバイスが接続されている。  */
		active_device = ATA_DEVICE0;
#if TNUM_ATA_DEVICE > 1
	} else if (device_type[ATA_DEVICE1] == ATA_DEVICE) {
		/*  デバイス1にATAデバイスが接続されている。  */
		active_device = ATA_DEVICE1;
#endif /* TNUM_ATA_DEVICE > 1 */
	} else {
		CHECK_ERR(E_OBJ);
		return E_OBJ;	/*  デバイスなし  */
	}
	ata_select_device(active_device);
	return E_OK;
}

/*
 *　シグネチャ取得
 *
 *　　引数：
 *　　　　ATA_DEVICE_NO device：デバイス番号
 *　　　　ATA_SIGNATURE *p_signature：デバイスのシグネチャを格納する領域
 *　　戻り値：
 *　　　　E_OK			正常終了
 *　　　　E_TMOUT		タイムアウト
 *　　　　E_OBJ			未接続、デバイス不良
 *
 *　　シグネチャはコマンド実行により、値が変化してしまうので、
 *　　リセット直後に読み出す
 *
 *　　未接続のデバイス1にアクセスを試みた場合、ホストやデバイスにより
 *　　応答が異なる。
 *　　　(1) statusレジスタが0xffと読み出せる→未接続
 *　　　(2) マスタ側（ダミーデバイス）のレジスタが読めてしまう。
 *　　　　　　そのダミーデバイスにコマンド実行しようとした場合
 *　　　　　　(2)-1 応答なし→タイムアウト
 *　　　　　　(2)-2 エラービットを立てて、コマンド終了
 */
static ER
ata_get_signature(ATA_DEVICE_NO device, ATA_SIGNATURE *p_signature)
{
	INT	i;
	UB	status, error;
	ER	ret;
	ATA_DEVICE_NO	current_device;
	
	assert(device < TNUM_ATA_DEVICE);
	
	for (i = 0; i < TMAX_RETRY_GET_SIGNATURE; i++) {
		/*  シグネチャにダミーデータを設定する  */
		p_signature->high = DUMMY_SIGNATURE;
		p_signature->low  = DUMMY_SIGNATURE;
		ata_select_device(device);
		status = ata_read_status();
		if (status == 0xff) {	/*  (1) 未接続  */
			return E_OBJ;
		}
		
		ret = ata_wait_bsy_clr();
		if (ret == E_TMOUT) {	/*  (2)-1 タイムアウト  */
			return E_TMOUT;
		}
		/*
		 *　Errorレジスタのチェック
		 *　　DEVICE RESETコマンドの戻り値
		 *
		 *　　　選択　　　　成功した場合の値
		 *　　　デバイス0　　0x01または0x81
		 *　　　デバイス1　　0x01
		 *
		 */
		error = ata_read_error();
		if (device == ATA_DEVICE0) {
			error &= ERROR_MASK;
		}
		if (error != 1) {	/*  (2)-2 デバイス不良  */
			return E_OBJ;
		}
		
		/*
		 *  シグネチャを読み出す前に指定されたデバイスが
		 *　実際に選択されているかをチェック
		 */
		current_device = ata_get_current_device();
		if (current_device == device) {
			p_signature->high = ata_reb_reg(ATA_CYL_HIGH);
			p_signature->low = ata_reb_reg(ATA_CYL_LOW);
			return E_OK;
		}
	}
	return E_TMOUT;
}


/*
 *　ATAデバイスタイプ取得
 *
 *　　引数：
 *　　　　ATA_DEVICE_NO device：デバイス番号
 *　　　　ATA_SIGNATURE *p_signature：デバイスのシグネチャ
 *　　　　UB *buff：デバイスのIDENTIFYデータを格納するメモリ領域へのポインタ
 *　　戻り値：
 *	　ATA_DEVICE		ATAデバイス
 *	　ATA_UNKNOWN		不明デバイス
 *	　ATA_NO_CONNECT	デバイス未接続
 */
static ATA_DEVICE_TYPE
ata_get_device_type(ATA_DEVICE_NO device, ATA_SIGNATURE *p_signature, UH *buff)
{
	ER 	err, err2;
	INT 	i;
	
	/*
	 *  シグネチャのチェック
	 *　　　ATAデバイスの場合、シグネチャはhigh,lowともに0x0
	 */
	if (!((p_signature->high == 0x0) && (p_signature->low == 0x0))) {
		return ATA_NO_CONNECT;	/*  ATAデバイスでない  */
	}
	/*  ATAデバイスの可能性あり  */

	/*
	 *　デバイスタイプ判定前は、デバイス・セレクション・プロトコルを
	 *　省略して、Device/Headレジスタを直接操作する。
	 */
	ata_select_device(device);
	active_device = device;
	
	err = ata_wait_bsy_clr();
	if (err != E_OK) {		/*  タイムアウト  */
		return ATA_UNKNOWN;
	}
	for(i = 0; i < TMAX_RETRY_IDENTIFY; i++) {
		err = ata_cmd_identify_device(device, buff);
		CHECK_ERR(err);
		dly_tsk(INTERVAL_INDETIFY_DEVICE);
		err2 = ata_cmd_identify_device(device, buff);
		CHECK_ERR(err2);
		if (err == err2) {	/*  2回とも同じ結果  */
			if (err == E_OK) {	
				/*  DEVICE IDENTIFYコマンド成功  */
				return ATA_DEVICE;
			} else {
				return ATA_UNKNOWN;
			}
		}
		dly_tsk(INTERVAL_INDETIFY_DEVICE);
	}
	return ATA_UNKNOWN;
}

