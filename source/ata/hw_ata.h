/*
 *  FatFs for TOPPERS
 *      FAT File system/
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 * 
 *  Copyright (C) 2005- by Industrial Technology Institute,
 *                          Miyagi Prefectural Government, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 */

/*
 *  ATAドライバ（PDIC相当）
 */

#ifndef _HW_ATA_H_
#define _HW_ATA_H_

#include "utils.h"
#include "ata_sil.h"
#include "ata_typedef.h"
#include "ata_cfg.h"		/*  TNUM_ATA_DEVICE  */

/*  ATAレジスタのアドレス定義はata_sil.hで行う。  */

/* レジスタのビット番号定義 */

/* (R) error register */
#define		ATA_E_ILI		0x01u	/* illegal length */
#define		ATA_E_NM		0x02u	/* no media */
#define		ATA_E_ABORT		0x04u	/* command aborted */
#define		ATA_E_MCR		0x08u	/* media change request */
#define		ATA_E_IDNF		0x10u	/* ID not found */
#define		ATA_E_MC		0x20u	/* media changed */
#define		ATA_E_UNC		0x40u	/* uncorrectable data */
#define		ATA_E_ICRC		0x80u	/* UDMA crc error */
#define		ATA_E_MASK		0x0fu	/* error mask */
#define		ATA_SK_MASK		0xf0u	/* sense key mask */
#define		ATA_SK_NO_SENSE		0x00u	/* no specific sense key info */
#define		ATA_SK_RECOVERED_ERROR	0x10u	/* command OK, data recovered */
#define		ATA_SK_NOT_READY	0x20u	/* no access to drive */
#define		ATA_SK_MEDIUM_ERROR	0x30u	/* non-recovered data error */
#define		ATA_SK_HARDWARE_ERROR	0x40u	/* non-recoverable HW failure */
#define		ATA_SK_ILLEGAL_REQUEST	0x50u	/* invalid command param(s) */
#define		ATA_SK_UNIT_ATTENTION	0x60u	/* media changed */
#define		ATA_SK_DATA_PROTECT	0x70u	/* write protect */
#define		ATA_SK_BLANK_CHECK	0x80u	/* blank check */
#define		ATA_SK_VENDOR_SPECIFIC	0x90u	/* vendor specific skey */
#define		ATA_SK_COPY_ABORTED	0xa0u	/* copy aborted */
#define		ATA_SK_ABORTED_COMMAND	0xb0u	/* command aborted, try again */
#define		ATA_SK_EQUAL		0xc0u	/* equal */
#define		ATA_SK_VOLUME_OVERFLOW	0xd0u	/* volume overflow */
#define		ATA_SK_MISCOMPARE	0xe0u	/* data dont match the medium */
#define		ATA_SK_RESERVED		0xf0u

/* (W) feature register */
#define		ATA_F_DMA		0x01u	/* enable DMA */
#define		ATA_F_OVL		0x02u	/* enable overlap */

/* (W) sector count */
/* (R) interrupt reason */
#define		ATA_I_CMD		0x01u	/* cmd (1) | data (0) */
#define		ATA_I_IN		0x02u	/* read (1) | write (0) */
#define		ATA_I_RELEASE		0x04u	/* released bus (1) */
#define		ATA_I_TAGMASK		0xf8u	/* tag mask */

/* Drive/Head register */
#define		ATA_D_DEV		0x10u	/* device0/1 */
#define		ATA_D_LBA		0x40u	/* use LBA addressing */
#define		ATA_D_IBM		0xa0u	/* 512 byte sectors, ECC */
#define		ATA_D_OBS		0xa0u	/* obs bit(bit7,5) */

/* status register */
#define		ATA_S_ERR		0x01u	/* error */
#define		ATA_S_INDEX		0x02u	/* index */
#define		ATA_S_CORR		0x04u	/* data corrected */
#define		ATA_S_DRQ		0x08u	/* data request */
#define		ATA_S_DSC		0x10u	/* drive seek completed */
#define		ATA_S_SERVICE		0x10u	/* drive needs service */
#define		ATA_S_DWF		0x20u	/* drive write fault */
#define		ATA_S_DMA		0x20u	/* DMA ready */
#define		ATA_S_DRDY		0x40u	/* drive ready */
#define		ATA_S_BSY		0x80u	/* busy */

/* alternate status register */
/* do for PCCARD devices */
/* device control register  */
#define		ATA_A_IDS		0x02u	/* disable interrupts */
#define		ATA_A_RESET		0x04u	/* RESET controller */
#define		ATA_A_4BIT		0x08u	/* 4 head bits */


/*  デバイス番号をDevice/HeadレジスタのDEVビットに変換  */
#define TO_DEV_BIT(device)	((UINT)(device) << 4u)

/*  デバイス番号の範囲チェック  */
#define CHECK_DEVICE_NO(device)	assert((device) < TNUM_ATA_DEVICE)

/*  ATAレジスタに書き込みを行った後の待ち時間[nsec]  */
#define ATA_400NSEC	400

/*  選択されているデバイス番号を取得  */
Inline ATA_DEVICE_NO 
ata_get_current_device(void) {
	UB head = ata_reb_reg(ATA_DRIVE);
	
	head &= ATA_D_DEV;
	if (head == 0) {
		return ATA_DEVICE0;
	} else {
		return ATA_DEVICE1;
	}
}

/*
 *　ソフトウェアリセット
 *　（かつ、割込み禁止）
 */
#define ata_software_reset() \
	ata_wrb_reg(ATA_DEV_CTRL, ATA_A_RESET | ATA_A_IDS)

/*
 *　ソフトウェアリセット解除
 *　（かつ、割込み禁止）
 */
#define ata_software_reset_negate() \
	ata_wrb_reg(ATA_DEV_CTRL, ATA_A_IDS)

/*
 *　Statusレジスタ読み出し
 */
#define ata_read_status()		ata_reb_reg(ATA_STATUS)

/*
 *　Alternate Statusレジスタの空読み
 *　　Statusレジスタが有効になる前にポーリングを行うのを防ぐ。
 */
#define ata_read_alternate_status()	ata_reb_reg(ATA_ALTSTAT)

/*
 *　Errorレジスタ読み出し
 */
#define ata_read_error()		ata_reb_reg(ATA_ERROR)

extern void ata_select_device(ATA_DEVICE_NO device);
extern void ata_execute_command(UB code);
extern ER ata_wait_bsy_clr(void);
extern ER ata_wait_bsy_drq_clr(void);
extern ER ata_wait_bsy0_drq1(void);
extern ER ata_wait_drdy_set(void);

/*
 *　エラーチェックマクロ
 */
#ifdef ATA_DEBUG
#define CHECK_ERR(err)	assert(err == E_OK)
#else /*  ATA_DEBUG  */
#define CHECK_ERR(err)
#endif /*  ATA_DEBUG  */


#endif /* _HW_ATA_H_ */
