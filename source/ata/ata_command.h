/*
 *  FatFs for TOPPERS
 *      FAT File system/
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 * 
 *  Copyright (C) 2005- by Industrial Technology Institute,
 *                          Miyagi Prefectural Government, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 */

/*
 *  ATAコマンド発行レイヤ
 */

#ifndef _ATA_COMMAND_H_
#define _ATA_COMMAND_H_

#include <t_services.h>
#include "ata_typedef.h"

/*
 *  SET FEATURESコマンド
 *　　Subcommand code：Set transfer mode
 */

/*  転送モード  */
#define PIO_DEFAULT		0x0	/*  PIO転送デフォルトモード  */
#define PIO_DEFAULT_NO_IORDY	0x1	/*  PIO転送デフォルトモード(IORDY無効）*/
#define PIO_FLOW_CTRL		0x4	/*  PIOフローコントロール転送モード  */
	/*  DMAモードについては未実装  */


extern ER ata_cmd_identify_device(ATA_DEVICE_NO device, UH *identify_data); 
extern ER ata_cmd_set_features_transfer_mode(ATA_DEVICE_NO device, 
						UB trnasfer_mode, UB trnasfer_mode_no);
extern ER ata_cmd_initialize_device_params(ATA_DEVICE_NO device, UB sector, UB head);
extern ER ata_cmd_read_sector(ATA_DEVICE_NO device, UW lba, UB sector, UH *buff);
extern ER ata_cmd_write_sector(ATA_DEVICE_NO device, UW lba, UB sector, const UH *buff);

#endif /* _ATA_COMMAND_H_ */
