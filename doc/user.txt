
            ＝ FatFs for TOPPERS ユーザズマニュアル ＝

            （Release 0.04対応，最終更新: 09-Feb-2007）

※ このユーザズマニュアルは，μITRON4.0仕様書（Ver. 4.02.00）やITRONデ
バイスドライバ設計ガイドラインの内容を前提に記述してあります．各ドキュ
メントは，以下のURLからダウンロードすることができます．

μITRON4.0仕様書：
        http://www.ertl.jp/ITRON/SPEC/mitron4-j.html

デバイスドライバ設計ガイドラインWG 中間報告書：
        http://www.ertl.jp/ITRON/GUIDE/device-j.html

----------------------------------------------------------------------
 FatFs for TOPPERS
     Toyohashi Open Platform for Embedded Real-Time Systems/
     FAT File System

 Copyright (C) 2006- by Takeshi Akamatu
 Copyright (C) 2007- by Industrial Technology Institute,
                             Miyagi Prefectural Government, JAPAN

 上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 によって公表されている GNU General Public License の Version 2 に記
 述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 利用と呼ぶ）することを無償で許諾する．
 (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
     権表示，この利用条件および下記の無保証規定が，そのままの形でソー
     スコード中に含まれていること．
 (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
     用できる形で再配布する場合には，再配布に伴うドキュメント（利用
     者マニュアルなど）に，上記の著作権表示，この利用条件および下記
     の無保証規定を掲載すること．
 (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
     用できない形で再配布する場合には，次のいずれかの条件を満たすこ
     と．
   (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
       作権表示，この利用条件および下記の無保証規定を掲載すること．
   (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
       報告すること．
 (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
     害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．

 本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 接的または間接的に生じたいかなる損害に関しても，その責任を負わない．

----------------------------------------------------------------------
 * μITRON4.0仕様は，トロン協会が中心となって策定されたオープンなリア
   ルタイムカーネル仕様です．μITRON4.0仕様の仕様書は，トロン協会のホー
   ムページ（http://www.assoc.tron.org/）から入手することができます．
----------------------------------------------------------------------
 * TRON は "The Real-time Operating system Nucleus" の略称です．
 * ITRON は "Industrial TRON" の略称です．
 * μITRON は "Micro Industrial TRON" の略称です．
 * TRON，ITRON，およびμITRONは，特定の商品ないしは商品群を指す名称で
   はありません．
 * TOPPERS は "Toyohashi OPen Platform for Embedded Real-time Systems" 
   の略称，JSP は "Just Standard Profile" の略称です．
 * 本マニュアル中の商品名は，各社の商標または登録商標です．
----------------------------------------------------------------------

/*--------------------------------------------------------------------------/
/  FatFs - FAT file system module  R0.04                     (C)ChaN, 2007
/---------------------------------------------------------------------------/
/ FatFs module is an experimenal project to implement FAT file system to
/ cheap microcontrollers. This is a free software and is opened for education,
/ research and development under license policy of following trems.
/
/  Copyright (C) 2007, ChaN, all right reserved.
/
/ * The FatFs module is a free software and there is no warranty.
/ * You can use, modify and/or redistribute it for personal, non-profit or
/   profit use without any restriction under your responsibility.
/ * Redistributions of source code must retain the above copyright notice.
/
/---------------------------------------------------------------------------/


１．FatFs for TOPPERSファイルシステムの概要

このソフトウェアは、赤松武史氏が開発し、
        http://elm-chan.org/fsw/ff/00index_j.html
でフリーソフトウェアとして公開しているファイルシステムFatFs(R0.04)を
ベースにTOPPERS/JSPカーネル上で動作するよう、デバイスドライバを追加し
たパッケージである。本パッケージの内容は以下のとおりである。

　ファイルシステム：FatFs(R0.04)
　カーネル：TOPPER/JSPカーネル Release1.4.2
　デバイスドライバ：
　　　　・PCカード・ドライバ
　　　　・ATAドライバ

ファイルシステムFatFsはその名が示す通り、FATファイルシステムをサポート
している。

２．ターゲットプロセッサ／ターゲットシステム

FatFs for TOPPERSファイルシステムは，以下のターゲットシステムで動作確
認を行った。

プロセッサ（型番）：SH3（SH7727）
ボード（メーカ名）：MS7727CP01（日立超LSIシステムズ）
PCカード・コントローラ：MR-SHPC-01 V2T-F（丸文）
PCカード・アダプタ：PCCF-ADP(I/O DATA）
記録メディア：
　コンパクト・フラッシュ・カード
　　　BUFFALO  32MB RCF-X
　　　SunDisk  64MB
　　　SunDisk 512MB
　　　SunDisk   2GB

　備考
　　CompactFlash以外の記録メディアについては「7.4　参考情報」を参照


開発環境：GNU開発環境
　・クロスツール
    　BINUTILS : 2.10.1
    　GCC-CORE : 2.95.3　（sh-hitachi-elf-gcc）
    　NEWLIB   : 1.9.0
　・セルフツール
    　GNU make : 3.80
    　GCC      : 3.4.4
    　PERL     : 5.8.7

ホストマシンの環境：
　　Windows2000 SP4
　　Cygwin 1.5.19


３．機能

ファイルシステムFatFsは以下の機能をサポートしている。

・FAT12, FAT16(+FAT64), FAT32に対応 (FAT64: FAT16 in 64KB/cluster) 
・8.3形式ファイル名とNT小文字フラグに対応
・FDISKフォーマット(基本区画)およびSFDフォーマットに対応
  (512B/sectorのみ)

3.1 制限事項
　　・FatFs自体の制限事項
　　　　・VFATには非対応
　　　　・セクタサイズは512バイト固定
　　　　・下位レイヤI/Fのディスクドライバ(disk_ioctl)の
　　　　　仕様が未公開？
　　・本配布パッケージの制限事項
　　　　・複数ドライブでの動作は未テスト
　　　　　　ターゲットシステムにドライブが１つしかないため
　　　　・下位レイヤI/Fのディスクドライバ(disk_ioctl)が未実装
　　　　　　・仕様が公開されていないため、未実装
　　　　　　・そのため、ディスクフォーマット機能は未テスト
　　　　・下位レイヤI/FのRTCドライバ(get_fattime)が未実装
　　　　・FatFsにはフルセットのFatFsと省メモリ版のTiny-FatFsがあるが、
　　　　　本パッケージではFatFsのみのサポートとする。

４．ディレクトリ構成

・用語
　　・GDIC(General Device Interface Compornet)：デバイス非依存部
　　・PDIC(Primitive Device Interface Compornet)：デバイス依存部
　　・SIL(System Interface Layer)：システム（ボード）依存部
　　　　詳細はデバイスドライバ設計ガイドラインを参照

toppers_fatfs
├─doc　ドキュメント
│  │  user.txt　　ユーザーズ・マニュアル
│  │  ata.txt　　 ATAドライバ・ユーザーズ・マニュアル
│  │  pcic.txt　　PCカード・ドライバ・ユーザーズ・マニュアル
│  └─fatfs_doc　　　 FatFs付属のユーザーズ・マニュアル
│          00index_j.html　FatFs付属のユーザーズ・マニュアル
│                          （C言語APIのインデックスを含む）
│          00index_e.html　上記00index_j.htmlの英語版
│      
├─jsp　　TOPPERS/JSPカーネル Release1.4.2 本体
│
├─sample　サンプル・プログラム
│      Makefile
│      sample1.cfg
│      sample1.h
│      sample1.c
│      
└─source　ソースコード（JSPカーネルに対して追加する部分）
    │  utils.h  ユーティリティ
    │  
    ├─fatfs　FatFs(R0.04)本体
    │      readme.txt　FatFs付属のREADMEファイル
    │      integer.h　データ型定義
    │      diskio.h　下位レイヤI/Fのインクルードファイル
    │      ff.c　　FatFs本体（フルセット）のソースファイル
    │      ff.h　　FatFs本体（フルセット）のインクルードファイル
    │      tff.c　Tiny-FatFs本体（省メモリ版）のソースファイル（参考）
    │      tff.h　Tiny-FatFs本体（省メモリ版）のインクルードファイル
    │  　　　　　　　　　　　　　　　　　　　　　　　　　　　（参考）
    ├─ata　　 ATAドライバ
    │      ata.c　　　　　　　　　 APIレイヤ
    │      ata.h
    │      ata_command.c　　　　　 ATAコマンド発行レイヤ
    │      ata_command.h
    │      ata_command_protocol.c　ATAコマンド・プロトコル・レイヤ
    │      ata_command_protocol.h
    │      ata_typedef.h　　　　　 データ型定義
    │      hw_ata.c　　　　　　　　ATAドライバのPDIC
    │      hw_ata.h
    │      
    ├─pcic　PCカード・ドライバ
    │  ├─gdic　PCカード・ドライバのGDIC
    │  │      pcic.c
    │  │      pcic.h
    │  │      
    │  └─pdic  PCカード・ドライバのPDIC
    │      └─mrshpc01v2t  丸文MR-SHPC-01 V2TのPDIC
    │              mrshpc01v2t.c
    │              mrshpc01v2t.h
　　│
    └─config　　 システム依存設定情報
    　  └─sh3_ms7727cp01　MS7727CP01依存情報
    　       ・FatFs関連
    　          Makefile.fatfs　
    　          　　MakefieのFatFs依存部
    　          ff_cfg.h　
    　          　　FatFsのコンフィギュレーション・パラメータ
    　          diskio_ata.c　
    　          　　FatFs用下位レイヤI/F
    　       ・ATAドライバ関連
    　          ata_cfg.h　
    　          　　ATAドライバのコンフィギュレーション・パラメータ
    　          ata_sil.h　
    　          　　ATAドライバのSIL
    　       ・PCカード・ドライバ関連
    　          hw_pcic.h　
    　          　　PCカードドライバのリネーム
    　          mrshpc01v2t_cfg.h　
    　          　　丸文MR-SHPC-01 V2TのPDIC用のコンフィギュレーション
    　          　　パラメータ
    　          mrshpc01v2t_sil.h
    　          　　丸文MR-SHPC-01 V2TのPDIC用のシステム
    　          　　インターフェース・レイヤ（SIL）
    　          pcic.cfg
    　          　　PCカードドライバのGDICのコンフィギュレーション
    　          　　ファイル
    　          pcic_cfg.h
    　          　　PCカードドライバのGDICのコンフィギュレーション
    　          　　パラメータ


備考： マニュアルの構成

デバイスドライバについてはそれぞれ別のマニュアルに分けてあるので、そち
らを参照のこと。

toppers_fatfs/doc
  ata.txt：ATAデバイス・ドライバ　ユーザーズ・マニュアル
  pcic.txt：PCカード・ドライバ　ユーザーズ・マニュアル

５．サンプルプログラムについて

5.1　サンプルプログラムの動作

toppers_fatfs/sampleにあるサンプルプログラムは、フルセット版FatFs
（ff.c, ff.h）のAPIの動作テストを行っている。記録メディアは、FAT16また
はFAT32でフォーマットされ、ファイルやディレクトリがまだ書き込まれてい
ない状態を想定している。
サンプルプログラム内で行っているテスト項目を以下に示す。

・f_write()のテスト
　ファイルdata.txtを書き込みモードでオープンする。
　クラスタ境界をまたいだ書き込みを行う。
　ファイルdata.txtをクローズする。

・f_read()のテスト
　ファイルdata.txtを読み出しモードでオープンする。
　クラスタ境界をまたいだ読み出しを行い、期待値と比較を行う。

・f_lseek()のテスト
　ファイルdata.txtを読み出しモードでオープンする。
　第2クラスタにシークする。
　シークした位置から読み出しを行い、期待値と比較する。

・f_mkdir(), f_opendir()のテスト
　/sub1, /sub1/sub2, sub1/sub2/sub3の順にディレクトリを作成し、
　開けることを確認する。

・f_readdir()のテスト
　メディアに記録されているすべてのファイル名とディレクトリ名を再帰
　的に読み出す。
　/data.txt, /sub1, /sub1/sub2, sub1/sub2/sub3が読み出せれば、成功。

・f_chmod(), f_stat()のテスト
　ファイルstat.txtを作成する。
　stat.txtのファイル属性を読み出す。
　stat.txtに読み取り専用のファイル属性を設定する。
　stat.txtのファイル属性を再度、読み出し、読み取り専用のファイル属
　性が設定されていることを確認する。（※）

　　※　記録メディアが脱着可能な場合はパソコン上でもファイル属性を
　　　　確認する。
　
・f_unlink()のテスト
　ファイルunlink.txtを作成する。
　unlink.txtを削除する。
　unlink.txtが削除されていることを確認する。（※）

　　※　記録メディアが脱着可能な場合はパソコン上でもファイルが削除
　　　　されていることを確認する。

・f_rename()のテスト
　ファイルold.txtを作成する。
　old.txtをnew.txtにリネームする。
　old.txtが存在しないことを確認する。（※）
　new.txtが存在することを確認する。　（※）

　　※　記録メディアが脱着可能な場合はパソコン上でもファイルが
　　　　リネームされていることを確認する。

・f_getfree()のテスト
　f_getfree()で空きクラスタ数を求める。
　空きクラスタ数をバイト数に換算して、ログ表示する。（※）
　
　　※　記録メディアが脱着可能な場合はパソコン上でも空き容量を
　　　　確認する。


テストが成功した場合のログを以下に示す。

JSP Kernel Release 1.4 (patchlevel = 2) for MS7727CP01(SH7727 T-Engine)
 (Feb  8 2007, 13:23:41)
Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
                            Toyohashi Univ. of Technology, JAPAN

System logging task is started on port 1.
Sample program starts (exinf = 0).
test write: OK
test read: OK
test seek: OK
test mkdir: OK
Dir: 0:/
File: 0:/data.txt
Dir: 0:/sub1/
Dir: 0:/sub1/sub2/
Dir: 0:/sub1/sub2/sub3/
Check read only attribute of file 0:/stat.txt by PC.
test stat: OK
test rename: OK
test unlink: OK
Waiting for f_getfree() .....
2043789312(0x79d1c000) bytes available on the disk.
　→この数値は、使用する記録メディアのサイズによって異なる。

Check free space by PC.
test getfree: OK
-- buffered messages --
Sample program ends.


5.2　ビルド手順

以下の手順はCygwinのコンソール上で行った。
サンプルプログラムが実行できない場合は、まず、FatFsを用いないケースで
JSPカーネル単体のサンプルプログラムが実行できるか動作確認を行い、問題
の切り分けを行って欲しい。

(0) 開発環境の構築

開発環境の構築についてはJSPカーネル SH3/SH4ターゲット依存部（GNU開発
環境）と同様である。
詳しくは以下のドキュメントを参照
　　toppers_fatfs/jsp/doc/ディレクトリ
　　　　user.txt：TOPPERS/JSPカーネル ユーザズマニュアル
　　　　sh3.txt：TOPPERS/JSPカーネル SH3/SH4 ターゲット依存部
　　　　　　　　　（GNU開発環境）ユーザーズ・マニュアル
　　　　gnu_install.txt：GNU開発環境構築マニュアル

(1) カーネルのコンフィギュレータをビルドする。

cd toppers_fatfs/jsp/cfg
make depend
make

この段階でビルド・エラーが発生する場合は、JSPカーネルのユーザーズ・マ
ニュアル（toppers_fatfs/jsp/doc/user.txt）を参照。


(2) サンプルプログラムをビルドする。

cd toppers_fatfs/sample
make depend
make

(3) ターゲットボードへの転送と実行

JSPカーネルの場合と同様。
なお、動作確認は（ROM化ではなく）ボード付属のモニタとJTAGデバッガを用
いて行った。


６．API一覧

FatFsの機能を呼び出すアプリケーションはff.hをインクルードする必要があ
る。（省メモリ版の場合はtff.h）
FatFsモジュールの使用を開始するにはまず、f_mount()を用いてFATFS型構造
体のワークエリアを論理ドライブに割り当てる。

初期化手順：
   static FATFS ActiveFatFsObj;  /*  ワークエリア  */
   f_mount(論理ドライブ番号, &ActiveFatFsObj);


6.1　データタイプ

FatFsで使用するデータ型は以下の通り。

(1) 基本データ型

これらはtoppers_fatfs/source/fatfs/intger.h内で定義している。

typedef UB      BYTE;   /*  符号なし8ビット整数  */
typedef UB      UCHAR;  /*  符号なし8ビット整数  */
typedef B       CHAR;   /*  符号付き8ビット整数  */
typedef UH      WORD;   /*  符号なし16ビット整数  */
typedef UH      USHORT; /*  符号なし16ビット整数  */
typedef H       SHORT;  /*  符号付き16ビット整数  */
typedef W       DWORD;  /*  符号なし32ビット整数  */
typedef unsigned long   ULONG;
typedef long            LONG;

(2) 目的別データ型

これらはtoppers_fatfs/source/fatfs/ff.hおよびtff.h内で定義している。

・サービスコールの戻り値
typedef unsigned char   FRESULT;


・ファイル・システム・オブジェクト構造体
typedef struct _FATFS {
        BYTE    fs_type;        /* FAT type */
        BYTE    sects_clust;    /* Sectors per cluster */
        BYTE    n_fats;         /* Number of FAT copies */
        BYTE    drive;          /* Physical drive number */
        WORD    id;             /* File system mount ID */
        WORD    n_rootdir;      /* Number of root directory entries */
        DWORD   winsect;        /* Current sector appearing in the win[] */
        DWORD   sects_fat;      /* Sectors per fat */
        DWORD   max_clust;      /* Maximum cluster# + 1 */
        DWORD   fatbase;        /* FAT start sector */
        DWORD   dirbase;        /* Root directory start sector 
                                        (cluster# for FAT32) */
        DWORD   database;       /* Data start sector */
        DWORD   last_clust;     /* Last allocated cluster */
        BYTE    winflag;        /* win[] dirty flag 
                                        (1:must be written back) */
        BYTE    win[512];       /* Disk access window for Directory/FAT */
} FATFS;


・ディレクトリ・オブジェクト構造体
typedef struct _DIR {
        FATFS*  fs;             /* Pointer to the owner file system object */
        DWORD   sclust;         /* Start cluster */
        DWORD   clust;          /* Current cluster */
        DWORD   sect;           /* Current sector */
        WORD    index;          /* Current index */
        WORD    id;             /* Sum of owner file system mount ID */
} DIR;


・ファイル・オブジェクト構造体
typedef struct _FIL {
        FATFS*  fs;             /* Pointer to the owner file system object */
        DWORD   fptr;           /* File R/W pointer */
        DWORD   fsize;          /* File size */
        DWORD   org_clust;      /* File start cluster */
        DWORD   curr_clust;     /* Current cluster */
        DWORD   curr_sect;      /* Current sector */
#if _FS_READONLY == 0
        DWORD   dir_sect;       /* Sector containing the directory entry */
        BYTE*   dir_ptr;        /* Ponter to the directory entry 
                                        in the window */
#endif
        WORD    id;             /* Sum of owner file system mount ID */
        BYTE    flag;           /* File status flags */
        BYTE    sect_clust;     /* Left sectors in cluster */
        BYTE    buffer[512];    /* File R/W buffer */
} FIL;


・ファイル状態構造体
typedef struct _FILINFO {
        DWORD fsize;            /* Size */
        WORD fdate;             /* Date */
        WORD ftime;             /* Time */
        BYTE fattrib;           /* Attribute */
        char fname[8+1+3+1];    /* Name (8.3 format) */
} FILINFO;


6.2　サービスコール一覧

(1) f_mount - ワークエリアの登録・削除 
(2) f_open - ファイルのオープン・作成 
(3) f_close - ファイルのクローズ 
(4) f_read - ファイルの読み込み 
(5) f_write - ファイルの書き込み 
(6) f_lseek - ファイルR/Wポインタの移動 
(7) f_sync - キャッシュされたデータのフラッシュ 
(8) f_opendir - ディレクトリのオープン 
(9) f_readdir - ディレクトリの読み出し 
(10) f_getfree - ディスク空き領域の取得 
(11) f_stat - ファイル・ステータスの取得 
(12) f_mkdir - ディレクトリの作成 
(13) f_unlink - ファイルまたはディレクトリの削除 
(14) f_chmod - ファイルまたはディレクトリ属性の変更 
(15) f_rename - ファイルまたはディレクトリの名前変更・移動 
(16) f_mkfs - ディスクのフォーマット 

6.2.1　各サービスコールのC言語API

各機能の詳細は
　toppers_fatfs/doc/fatfs_doc/00index_j.html
内の「上位レイヤI/F」を参照。
ファイル名やディレクトリの記述形式は
　toppers_fatfs/doc/fatfs_doc/ja/filename.html
を参照。

(1) f_mount - ワークエリアの登録・削除

【C言語API】
　FRESULT f_mount (BYTE  Drive, FATFS*  FileSystemObject);

【パラメータ】
  BYTE  Drive,               論理ドライブ番号
  FATFS*  FileSystemObject   ワーク・エリアへのポインタ

(2) f_open - ファイルのオープン・作成

【C言語API】
　FRESULT f_open (FIL* FileObject, const char* FileName, 
　　　　　　　　　BYTE ModeFlags);

【パラメータ】
  FIL* FileObject       作成するファイルオブジェクト構造体へのポインタ
  const char* FileName  ファイルのフルパス名へのポインタ
  BYTE ModeFlags        モードフラグ
  　　FA_READ
  　　　　読み出しモードでオープンする。読み書きする場合はFA_WRITEと
  　　　　共に指定する。
  　　FA_WRITE 
  　　　　書き込みモードでオープンする。読み書きする場合はFA_READと共
  　　　　に指定する。 
  　　FA_OPEN_EXISTING 
  　　　　既存のファイルを開く。ファイルが無いときはエラーになる。 
  　　FA_CREATE_ALWAYS 
  　　　　ファイルを作成する。既存のファイルがある場合は、サイズを0に
  　　　　してから開く。 
  　　FA_OPEN_ALWAYS
  　　　　 既存のファイルを開く。ファイルが無いときはファイルを作成す
  　　　　 る。 

(3) f_close - ファイルのクローズ

【C言語API】
　FRESULT f_close (FIL* FileObject);

【パラメータ】
  FIL* FileObject      ファイルオブジェクト構造体へのポインタ


(4) f_read - ファイルの読み込み

【C言語API】
  FRESULT f_read (FIL* FileObject, void* Buffer, WORD ByteToRead,
                 WORD* ByteRead);

【パラメータ】
  FIL* FileObject     ファイルオブジェクト構造体
  void* Buffer        読み出したデータを格納するバッファ
  WORD ByteToRead     読み出すバイト数
  WORD* ByteRead      読み出されたバイト数


(5) f_write - ファイルの書き込み

【C言語API】
　FRESULT f_write (FIL* FileObject, const void* Buffer, 
　                              WORD ByteToWrite, WORD* ByteWritten);

【パラメータ】
  FIL* FileObject      ファイルオブジェクト構造体
  const void* Buffer   書き込みデータ
  WORD ByteToWrite     書き込むバイト数
  WORD* ByteWritten    書き込まれたバイト数


(6) f_lseek - ファイルR/Wポインタの移動

【C言語API】
　FRESULT f_lseek (FIL* FileObject, DWORD Offset);
　
【パラメータ】
  FIL* FileObject    ファイルオブジェクト構造体へのポインタ
  DWORD Offset       移動先オフセット
  　オフセットはファイル先頭からの値として与える。現在位置からの相対値
  　で指定したい場合は、ファイルオブジェクト構造体のメンバfptrを用いる
  　と相対位置から絶対位置に換算できる。
  　
  　　絶対オフセット　＝　現在位置（FileObject->fptr）＋ 相対オフセット


(7) f_sync - キャッシュされたデータのフラッシュ

【C言語API】
  FRESULT f_sync (FIL* FileObject);

【パラメータ】
  FIL* FileObject     ファイルオブジェクト構造体へのポインタ


(8) f_opendir - ディレクトリのオープン

【C言語API】
　FRESULT f_opendir (DIR* DirObject, const char* DirName);

【パラメータ】
  DIR* DirObject       ディレクトリブジェクト構造体へのポインタ
  const char* DirName  ディレクトリ名へのポインタ


(9) f_readdir - ディレクトリの読み出し

【C言語API】
　FRESULT f_readdir (DIR* DirObject, FILINFO* FileInfo);

【パラメータ】
  DIR* DirObject     ディレクトリブジェクト構造体へのポインタ
  FILINFO* FileInfo  ファイル情報構造体へのポインタ

　　　
(10) f_getfree - ディスク空き領域の取得

【C言語API】
　FRESULT f_getfree (DWORD* Clusters);

【パラメータ】
  DWORD* Clusters     空きクラスタ数を格納する変数へのポインタ
  

(11) f_stat - ファイル・ステータスの取得

【C言語API】
　FRESULT f_stat (const char* FileName, FILINFO* FileInfo);

【パラメータ】
  const char* FileName  ファイルまたはディレクトリ名へのポインタ
  FILINFO* FileInfo     ファイル情報構造体へのポインタ


(12) f_mkdir - ディレクトリの作成

【C言語API】
　FRESULT f_mkdir (const char* DirName);
　
【パラメータ】
  const char* DirName  作成するディレクトリ名へのポインタ


(13) f_unlink - ファイルまたはディレクトリの削除

【C言語API】
　FRESULT f_unlink (const char* FileName);
　
【パラメータ】
  const char* FileName   ファイルまたはディレクトリ名へのポインタ


(14) f_chmod - ファイルまたはディレクトリ属性の変更

【C言語API】
　FRESULT f_chmod (const char* FileName, BYTE Attribute,
                   BYTE AttributeMask);

【パラメータ】
  const char* FileName  ファイルまたはディレクトリの文字列へのポインタ
  BYTE Attribute        設定値
  　　AM_RDO リードオンリー 
  　　AM_ARC アーカイブ 
  　　AM_SYS システム 
  　　AM_HID 隠しファイル
  BYTE AttributeMask    変更マスク
　　　変更マスクでセットされているビットだけが変更対象になる。


(15) f_rename - ファイルまたはディレクトリの名前変更・移動

【C言語API】
　FRESULT f_rename (const char* OldName, const char* NewName)

【パラメータ】
  const char* OldName  古いファイルまたはディレクトリの文字列へのポインタ
  const char* NewName  新しいファイルまたはディレクトリの文字列へのポインタ


(16) f_mkfs - ディスクのフォーマット

【C言語API】
　FRESULT f_mkfs (BYTE Drive, BYTE PartitioningRule, BYTE AllocSize);

【パラメータ】
  BYTE  Drive            フォーマットする論理ドライブ(0-9)。
  BYTE  PartitioningRule パーティション・ルール
  　　　　　　　　　　　　０：FDISKフォーマット
  　　　　　　　　　　　　１：super floppy (SFD) フォーマット
  BYTE  AllocSize        アロケーション・ユニット・サイズ
  　　　　　　　　　　　　(クラスタ・サイズ)をセクタ単位で指定する。


6.3　コンフィギュレーション・オプション

FatFsは以下のマクロ定義により、コンフィギュレーションできるようになっ
ている。コンフィギュレーションにより不要な機能を削除し、コードサイズを
削減することができる。
定義場所はシステム依存部のff_cfg.hである。

(1) _MCU_ENDIAN
   _MCU_ENDIANはFAT構造にアクセスする方法を定義する。
   　1: ワード・アクセス可能
   　2: ワード・アクセス不可能
   
   プロセッサが以下のいずれかに該当する場合は_MCU_ENDIANを2に定義する
   必要がある。
   そうでなければ、このマクロを1に定義すればよく、その場合、コードサ
   イズを削減できる。
 
   - プロセッサのバイト・オーダーがビッグエンディアンである。
   - アラインに合っていないメモリ・アクセスが禁止されている。


(2) _FS_READONLY
   _FS_READONLYを1に設定するとリード・オンリー構成となる。
   このとき以下の書き込み機能が削除される。
   　・f_write, f_sync, f_unlink, f_mkdir, f_chmod, f_rename 
   　・不要なf_getfree


(3) _FS_MINIMIZE
   _FS_MINIMIZEオプションでは、最小化レベルを定義し、以下の機能が
   選択される。
   　0:全機能が有効
   　1:f_stat, f_getfree, f_unlink, f_mkdir, f_chmod, f_renameを削除
   　2:1に加えて、f_opendir, f_readdirを削除


(4) _USE_SJIS（未テスト）
  _USE_SJISが定義されると、ファイル名としてシフトJISコードが使用できる。
  このマクロはデフォルトで定義されている。

(5) _USE_MKFS（未テスト）
   _USE_MKFSが定義され、_FS_READONLYが0のとき、f_mkfsの機能が使用でき
   る。

コンフィギュレーション・オプションにより、どの関数が削除されるかを以下
に示す。

               _FS_MINIMIZE  _FS_MINIMIZE  _FS_READONLY  _USE_MKFS
                    (1)           (2)          (1)        (undef)
   f_mount
   f_open
   f_close
   f_read
   f_lseek
   f_write                                     x
   f_sync                                      x
   f_opendir                      x
   f_readdir                      x
   f_stat            x            x
   f_getfree         x            x            x
   f_unlink          x            x            x
   f_mkdir           x            x            x
   f_chmod           x            x            x
   f_rename          x            x            x
   f_mkfs                                      x            x



７．移植者向けの情報

7.1　下位レイヤI/F

FatFs/Tiny-FatFsモジュールは、物理ドライブへのアクセスや現在時刻を得
るため、下位レイヤに次のインターフェースを要求する。
詳細はtoppers_fatfs/doc/fatfs_doc/00index_j.html内の「下位レイヤI/F」
を参照。

7.1.1 データ型

下位レイヤI/Fでは、以下のデータ型を用いており、
toppers_fatfs/source/fatfs/diskio.hで定義している。

typedef unsigned char   DSTATUS;  /*  ディスクステータス  */
typedef unsigned char   DRESULT;  /*  リターンパラメータ  */


7.1.2 ディスクI/Oモジュール

サービスコール一覧
(1) disk_initialize - ディスク・ドライブの初期化
(2) disk_status - ディスク・ドライブの状態取得
(3) disk_read - ディスクからの読み込み（セクタ単位）
(4) disk_write - ディスクへの書き込み（セクタ単位）
(5) disk_ioctl - I/Oコントロール（仕様未公開？）

API詳細

(1) disk_initialize - ディスク・ドライブの初期化

【C言語API】
　DSTATUS disk_initialize(BYTE Drive);

【パラメータ】
　BYTE Drive           物理ドライブ番号
　　　
【リターンパラメータ】
　この関数は戻り値としてディスクステータスを返す。ディスクステータス
　の詳細に関してはdisk_status()を参照。

(2) disk_status - ディスク・ドライブの状態取得

【C言語API】
　DSTATUS disk_status(BYTE Drive);

【パラメータ】
　BYTE Drive           物理ドライブ番号
　　　
【リターンパラメータ】
　ディスクドライブの状態が次のフラグの組み合わせの値で返される。

　　STA_NOINIT 
　　　ドライブが初期化されていないことを示すフラグ。電源ONまたはメディ
　　　アの取り外しでセットされ、disk_initialize()の正常終了でクリア、
　　　失敗でセットされる。 
　　STA_NODISK 
　　　メディアがセットされていないことを示すフラグ。メディアが取り外さ
　　　れている間はセットされ、メディアがセットされている間はクリアされ
　　　る。固定ディスクでは常にクリアされている。 
　　STA_PROTECTED 
　　　メディアがライトプロテクトされていることを示すフラグ。ライトプロ
　　　テクトノッチをサポートしないメディアでは常にクリアされている。
　　　 

(3) disk_read - ディスクからの読み込み（セクタ単位）

【C言語API】
　DRESULT disk_read(BYTE Drive, BYTE* Buffer, DWORD SectorNumber,
　                  BYTE SectorCount);

【パラメータ】
  BYTE Drive　　　　　 物理ドライブ番号
  BYTE* Buffer         読み出しバッファへのポインタ
                       SectorCount * 512バイトのサイズが必要である。 
  DWORD SectorNumber   読み出し開始セクタ番号。 LBAで指定する。
  BYTE SectorCount     読み出すセクタ数。 1〜255で設定する。

【リターンパラメータ】
　RES_OK 
　　　正常終了。 
　RES_ERROR 
　　　読み込み中にエラーが発生した。 
　RES_PARERR 
　　　パラメータが不正。 
　RES_NOTRDY 
　　　ドライブが動作可能状態ではない（初期化されていない）。 


(4) disk_write - ディスクへの書き込み（セクタ単位）

【C言語API】
　DRESULT disk_write (BYTE Drive, const BYTE* Buffer,  DWORD SectorNumber,
                      BYTE SectorCount);

【パラメータ】
  BYTE Drive　　　　　 物理ドライブ番号
  const BYTE* Buffer   書き込むデータへのポインタ
  DWORD SectorNumber   書き込み開始セクタ番号。LBAで指定する。
  BYTE SectorCount     書き込むセクタ数。 1〜255で設定する。
　　　
【リターンパラメータ】
　RES_OK 
　　　正常終了。 
　RES_ERROR 
　　　書き込み中にエラーが発生した。 
　RES_WRPRT 
　　　ディスクが書き込み禁止状態。 
　RES_PARERR 
　　　パラメータが不正。 
　RES_NOTRDY 
　　　ドライブが動作可能状態ではない（初期化されていない）。

【備考】
　　リードオンリー構成ではこの関数は必要とされない。

7.1.3 RTCモジュール

(1) get_fattime - 日付・時刻の取得

【C言語API】
　DWORD get_fattime (void);

【パラメータ】なし
　　　
【リターンパラメータ】
　現在のローカルタイムがDWORD値にパックされて返される。
　ビットフィールドは次に示すようになる。

　bit31:25 
　　　1980年を起点とした年が 0..127 で入る。 
　bit24:21 
　　　月が 1..12 の値で入る。 
　bit20:16 
　　　日が 1..31 の値で入る。 
　bit15:11 
　　　時が 0..23 の値で入る。 
　bit10:5 
　　　分が 0..59 の値で入る。 
　bit4:0 
　　　秒/2が 0..29 の値で入る。 

【備考】
　RTCをサポートしないシステムでも、何らかの有効な値を返さなければなら
　ない。リードオンリー構成ではこの関数は必要とされない。

7.2　処理系に対する仮定

FatFs for TOPPERSファイルシステムは、処理系に対して以下の事項を仮定し
ている。

・以下のデータ型をサポートしている
　　・8ビット符号なし整数
　　・16ビット符号なし整数
　　・32ビット符号なし整数
・整数の内部表現に２の補数を用いている。
・標準ライブラリとして以下の関数がサポートされている。
　　・memcpy()
　　・memset()
　　・memcmp()
　　・strncmp()   （※）
　　・strlen()    （※）
　　・sprintf()   （※）
　　　
　　（※）サンプルプログラム内で使用しているだけで、FatFs自体は使用し
　　　　　ていない。（必須ではない。）

7.3 ミドルウェア用の Makefile

TOPPERS/JSPカーネルでは、Release1.4.2から、ミドルウェアを組み合わせて
ビルドすることを考慮して、Makefileの形式を変更している。本配布パッケー
ジもこのMakefileの形式に合わせている。
toppers_fatfs/source/config/sh3_ms7727cp01/Makefile.fatfs
がそのMakefileに相当する。

このMakefile内で定義すべき変数を以下に示す。
（なお、FatFsをライブラリ形式にするビルド方法は動作確認していない。）

(1) MTASK_CFG
　　　ミドルウェアのコンフィギュレーションファイル（ソース）を追加する．

(2) MTASK_KERNEL_CFG
　　　ミドルウェアのコンフィギュレータから出力され，JSPカーネル のシス
　　　テムコンフィギュレーションファイルにインクルードされるファイルを
　　　追加する．

(3) MTASK_DIR
　　　ミドルウェアのディレクトリを追加する．

(4) MTASK_LCSRCS
　　　ミドルウェアをライブラリ化するソースファイルを追加する．

(5) MTASK_ASMOBJS
　　　ミドルウェアのアセンブリ言語のオブジェクトファイルを追加する．

(6) MTASK_CXXOBJS
　　　ミドルウェアの C++ 言語のオブジェクトファイルを追加する．

(7) MTASK_COBJS
　　　ミドルウェアの C 言語のオブジェクトファイルを追加する．

(8) MTASK_CFLAGS
　　　ミドルウェアをコンパイルするときのオプションを指定する．

(9) MTASK_LIBS
　　　ミドルウェアのライブラリを追加する．

(10) MAKE_MTASK
　　　ライブラリ化したミドルウェアを指定する．

7.4　参考情報

　CompactFlash以外の記録メディア（MMC, ATA HDD）についてはFatFs原作者
の赤松氏が下記のURLで「サンプル・プロジェクト」を公開している。
他のデバイスに移植する場合には参考にされたい。

        http://elm-chan.org/fsw/ff/00index_j.html


８．その他

8.1 ウェブサイト

TOPPERSプロジェクトでは公式ウェブサイトを，以下のURLに用意している．

        http://www.toppers.jp/

配付キットの最新版は，このウェブサイトからたどることができる．
また，後述のメーリングリストのアーカイブなども，このウェブサイトで閲覧
することができる．

8.2 利用条件・著作権

FatFs for TOPPERSファイルシステムの利用条件は，各ファイルの先頭に明示
されている（このドキュメントの先頭にもついている）．著作権は，各ファイ
ルの先頭に表示されている著作権者が保有している．

利用条件の (3) の (b) において，利用の形態をTOPPERSプロジェクトに報告
する方法としては，FatFs for TOPPERSファイルシステムを利用した製品の種
別と名称，利用したFatFs for TOPPERSファイルシステムの構成を，次のアド
レスへメールで連絡する方法を標準とする．
FatFs for TOPPERSファイルシステムを製品以外に利用した場合には，それに
準じる情報を報告するものとする．

        report@toppers.jp

またその際に，FatFs for TOPPERSファイルシステムを使用してのコメントや
ご意見もいただけると幸いである．この方法での報告が難しい場合には，別途
相談されたい．

8.3 保証・サポート・適用性

FatFs for TOPPERSファイルシステムは無保証で提供されているものである．
開発者は，その適用可能性も含めて，いかなる保証も行わない．また，サポー
トの約束もしていない．
質問がある場合は，後述のメーリングリストを利用していただけると幸いであ
る．

8.4 メーリングリスト

配布キットにバグや問題点を発見した場合には，TOPPERSユーザーズ・メーリ
ングリスト（toppers-users）に報告して欲しい．
TOPPERSユーザーズ・メーリングリストについては下記のURLを参照されたい。

　　　　http://www.toppers.jp/community.html

サンプルプログラムが実行できない場合は、まず、FatFsを用いない状態でJSP
カーネル単体のサンプルプログラムが実行できるか動作確認を行い、問題の切
り分けを行って欲しい。

メーリングリストにバグや問題点などを報告する場合には，次の情報を知らせ
て欲しい．

    ターゲットに関する情報
        ・ターゲットプロセッサの種類
        ・ターゲットボードの種類
        ・記録メディアの種類
        ・プロセッサと記録メディアの接続方法

    ホストに関する情報
        ・OSのバージョン（サービスパックの適用状況も）
        ・コンパイラなどの開発環境のバージョン
        　（Cygwinの場合はそのバージョンも）
　　　　　　備考：
　　　　　　　　Cygwinのバージョンの確認方法
　　　　　　　　　　uname -r　コマンドを実行する。
　　　　　　　　　　

8.5 TOPPERSプロジェクトへの参加

TOPPERSプロジェクトでは，何からの形でプロジェクトに貢献されたい方，プ
ロジェクトで開発したソフトウェアをお使いの方，プロジェクトに興味をお持
ちの方の参加を求めている．TOPPERSプロジェクトへの参加方法については，
TOPPERSプロジェクトのウェブサイトを参照すること．


バージョン履歴

    2007年2月 5日      Release 0.03a           最初のリリース
                       （TOPPERSプロジェクト内早期リリース）
    2007年2月 9日      Release 0.04           
                       （TOPPERSプロジェクト内早期リリース）
                       FatFs(R0.04)対応
                       　FatFs(R0.03a)からFatFs(R0.04)への変更点
                       　　・複数ドライブ対応
                       　　・ディスク・フォーマット機能のサポート
    2007年3月 1日      一般公開

以上
