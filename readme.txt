----------------------------------------------------------------------
 FatFs for TOPPERS
     Toyohashi Open Platform for Embedded Real-Time Systems/
     FAT File System

 Copyright (C) 2006- by Takeshi Akamatu
 Copyright (C) 2007- by Industrial Technology Institute,
                             Miyagi Prefectural Government, JAPAN

 上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 によって公表されている GNU General Public License の Version 2 に記
 述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 利用と呼ぶ）することを無償で許諾する．
 (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
     権表示，この利用条件および下記の無保証規定が，そのままの形でソー
     スコード中に含まれていること．
 (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
     用できる形で再配布する場合には，再配布に伴うドキュメント（利用
     者マニュアルなど）に，上記の著作権表示，この利用条件および下記
     の無保証規定を掲載すること．
 (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
     用できない形で再配布する場合には，次のいずれかの条件を満たすこ
     と．
   (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
       作権表示，この利用条件および下記の無保証規定を掲載すること．
   (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
       報告すること．
 (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
     害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．

 本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 接的または間接的に生じたいかなる損害に関しても，その責任を負わない．

----------------------------------------------------------------------
 * μITRON4.0仕様は，トロン協会が中心となって策定されたオープンなリア
   ルタイムカーネル仕様です．μITRON4.0仕様の仕様書は，トロン協会のホー
   ムページ（http://www.assoc.tron.org/）から入手することができます．
----------------------------------------------------------------------
 * TRON は "The Real-time Operating system Nucleus" の略称です．
 * ITRON は "Industrial TRON" の略称です．
 * μITRON は "Micro Industrial TRON" の略称です．
 * TRON，ITRON，およびμITRONは，特定の商品ないしは商品群を指す名称で
   はありません．
 * TOPPERS は "Toyohashi OPen Platform for Embedded Real-time Systems" 
   の略称，JSP は "Just Standard Profile" の略称です．
 * 本マニュアル中の商品名は，各社の商標または登録商標です．
----------------------------------------------------------------------

FatFs for TOPPERS (Release 0.04)

・概要

FatFsは、赤松武史氏が開発し、フリーソフトウェアとして公開し
ているFAT仕様準拠のファイルシステムです。
本パッケージはFatFs(R0.04)をベースにTOPPERS/JSPカーネル上で
動作するよう、デバイスドライバを追加したパッケージです。本
パッケージの内容は以下の通りです。

　ファイルシステム：FatFs(R0.04)
　カーネル：TOPPER/JSPカーネル Release1.4.2
　デバイスドライバ：
　　　　・PCカード・ドライバ
　　　　・ATAドライバ

・ターゲットプロセッサ／ターゲットシステム

FatFs for TOPPERSは，以下のターゲットシステムで動作確認を行
いました。

プロセッサ（型番）：SH3（SH7727）
ボード（メーカ名）：MS7727CP01（日立超LSIシステムズ）
PCカード・コントローラ：MR-SHPC-01 V2T-F（丸文）
PCカード・アダプタ：PCCF-ADP(I/O DATA）
記録メディア：
　コンパクト・フラッシュ・カードCF

・機能

FatFsはファイルシステムとして、以下の機能をサポートしています。

・FAT12, FAT16(+FAT64), FAT32に対応 (FAT64: FAT16 in 64KB/cluster) 
・8.3形式ファイル名とNT小文字フラグに対応
・FDISKフォーマット(基本区画)およびSFDフォーマットに
　対応(512B/sectorのみ)

・サービスコール一覧

FatFsは以下のサービスコールをサポートしています。

(1) f_mount - ワークエリアの登録・削除 
(2) f_open - ファイルのオープン・作成 
(3) f_close - ファイルのクローズ 
(4) f_read - ファイルの読み込み 
(5) f_write - ファイルの書き込み 
(6) f_lseek - ファイルR/Wポインタの移動 
(7) f_sync - キャッシュされたデータのフラッシュ 
(8) f_opendir - ディレクトリのオープン 
(9) f_readdir - ディレクトリの読み出し 
(10) f_getfree - ディスク空き領域の取得 
(11) f_stat - ファイル・ステータスの取得 
(12) f_mkdir - ディレクトリの作成 
(13) f_unlink - ファイルまたはディレクトリの削除 
(14) f_chmod - ファイルまたはディレクトリ属性の変更 
(15) f_rename - ファイルまたはディレクトリの名前変更・移動 
(16) f_mkfs - ディスクのフォーマット 


・ライセンス

FatFs本体は非常に緩いライセンスになっています。
本パッケージを利用する場合は、TOPPERSライセンスおよび下記の
ライセンスに従ってください。

***************************************************************
FatFs本体のコピーライト
***************************************************************

/*--------------------------------------------------------------------------/
/  FatFs - FAT file system module  R0.04                     (C)ChaN, 2007
/---------------------------------------------------------------------------/
/ FatFs module is an experimenal project to implement FAT file system to
/ cheap microcontrollers. This is a free software and is opened for education,
/ research and development under license policy of following trems.
/
/  Copyright (C) 2007, ChaN, all right reserved.
/
/ * The FatFs module is a free software and there is no warranty.
/ * You can use, modify and/or redistribute it for personal, non-profit or
/   profit use without any restriction under your responsibility.
/ * Redistributions of source code must retain the above copyright notice.
/
/---------------------------------------------------------------------------/

***************************************************************
FatFs/Tiny-FatFsモジュールはフリーソフトウェアとして教育・研
究・開発用に公開しています。どのような利用目的（個人・非商
用・商用）でも使用・改変・配布について一切の制限はありません
が、全て利用者の責任の下での利用とします。
ソースコードの再配布では上記のコピーライトを残さなければいけ
ません。
***************************************************************

使い方の詳細は、配布パッケージに含まれるユーザーズ・マニュアル
toppers_fatfs/doc/user.txtを参照して下さい。

FatFsの公開ページ
	http://elm-chan.org/fsw/ff/00index_j.html
