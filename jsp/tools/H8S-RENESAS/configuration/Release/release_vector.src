;
;  TOPPERS/JSP Kernel
;      Toyohashi Open Platform for Embedded Real-Time Systems/
;      Just Standard Profile Kernel
;
;  Copyright (C) 2000-2004 by Embedded and Real-Time Systems Laboratory
;                              Toyohashi Univ. of Technology, JAPAN
;  Copyright (C) 2001-2004 by Industrial Technology Institute,
;                              Miyagi Prefectural Government, JAPAN
;  Copyright (C) 2001-2004 by Dep. of Computer Science and Engineering
;                   Tomakomai National College of Technology, JAPAN
;
;  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation
;  によって公表されている GNU General Public License の Version 2 に記
;  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
;  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
;  利用と呼ぶ）することを無償で許諾する．
;  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
;      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
;      スコード中に含まれていること．
;  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
;      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
;      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
;      の無保証規定を掲載すること．
;  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
;      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
;      と．
;    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
;        作権表示，この利用条件および下記の無保証規定を掲載すること．
;    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
;        報告すること．
;  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
;      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
;
;  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
;  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
;  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
;  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
;
;  @(#) $Id: release_vector.src,v 1.2 2005/12/12 09:20:18 honda Exp $
;

;
; ベクタテーブルと割込みの入口処理
;　　　割込み制御モード２用
;
        .CPU 2000A

;
;　　割込みの入口処理を生成するマクロの定義
;　　　（割込み要因毎に異なる部分）
;
;　　　パラメータ
;　　　　　C_ROUTINE：C言語ルーチンの関数名（先頭の'_'は付けない）
;
;　　　レジスタ割り当て
;　　　　　・er0：C言語ルーチンの先頭アドレス
;
;
;　　　処理内容
;　　　　　er0を退避後、C言語ルーチンの先頭アドレスを設定して
;　　　　　すべての割込み要因共通の処理へ分岐する。
;
;　　　備考
;　　　　　割込み制御モード２では割込み受付直後に割込み禁止になっていない。
;　　　　　そのため、入口処理の途中で多重割込みが入る可能性がある。
;　　　　　本実装では、割込み応答性を良くするため、割込み禁止のタイミングを
;　　　　　できるだけ遅らせている。
;
;
;　他機種と共通の注意事項
;　　reqflg をチェックする前に割込みを禁止しないと，reqflg をチェック
;　　後に起動された割込みハンドラ内でディスパッチが要求された場合に，
;　　ディスパッチされない．
;
 .MACRO INTHDR_ENTRY C_ROUTINE
        .IMPORT _\C_ROUTINE
__kernel_\C_ROUTINE'_entry:
        push.l  er0                             ;　レジスタの退避
        mov.l   #_\C_ROUTINE, er0               ;　C言語ルーチンの先頭アドレス
        jmp     @_common_interrupt_process      ;　共通の処理へ分岐

 .ENDM


; ベクタテーブルの定義

        .SECTION P, CODE, ALIGN=2

        .INCLUDE "sys_config.inc"
        .INCLUDE "cpu_config.inc"

        .IMPORT _start
        .IMPORT _no_reg_exception
        .IMPORT _common_interrupt_process

;
; 割込みベクタの定義
;  （ベクタテーブルは固定なので、割込みの出入り口処理のラベルは
;   以下のベクタテーブルに直接書き込む必要がある。)
;
        .SECTION V, CODE, ALIGN=4

        .GLOBAL _vectors
_vectors:
;******************************************************
;　ここから下はコンフィギュレーション時に自動生成される
;******************************************************

	.DATA.L _start	;  0, 0x00
	.DATA.L _no_reg_exception	;  1, 0x01
	.DATA.L _no_reg_exception	;  2, 0x02
	.DATA.L _no_reg_exception	;  3, 0x03
	.DATA.L _no_reg_exception	;  4, 0x04
	.DATA.L _no_reg_exception	;  5, 0x05
	.DATA.L _no_reg_exception	;  6, 0x06
	.DATA.L _no_reg_exception	;  7, 0x07
	.DATA.L _no_reg_exception	;  8, 0x08
	.DATA.L _no_reg_exception	;  9, 0x09
	.DATA.L _no_reg_exception	;  10, 0x0a
	.DATA.L _no_reg_exception	;  11, 0x0b
	.DATA.L _no_reg_exception	;  12, 0x0c
	.DATA.L _no_reg_exception	;  13, 0x0d
	.DATA.L _no_reg_exception	;  14, 0x0e
	.DATA.L _no_reg_exception	;  15, 0x0f
	.DATA.L _no_reg_exception	;  16, 0x10
	.DATA.L _no_reg_exception	;  17, 0x11
	.DATA.L _no_reg_exception	;  18, 0x12
	.DATA.L _no_reg_exception	;  19, 0x13
	.DATA.L _no_reg_exception	;  20, 0x14
	.DATA.L _no_reg_exception	;  21, 0x15
	.DATA.L _no_reg_exception	;  22, 0x16
	.DATA.L _no_reg_exception	;  23, 0x17
	.DATA.L _no_reg_exception	;  24, 0x18
	.DATA.L _no_reg_exception	;  25, 0x19
	.DATA.L _no_reg_exception	;  26, 0x1a
	.DATA.L _no_reg_exception	;  27, 0x1b
	.DATA.L _no_reg_exception	;  28, 0x1c
	.DATA.L _no_reg_exception	;  29, 0x1d
	.DATA.L _no_reg_exception	;  30, 0x1e
	.DATA.L _no_reg_exception	;  31, 0x1f
	.DATA.L __kernel_timer_handler_entry	;  32, 0x20
	.DATA.L _no_reg_exception	;  33, 0x21
	.DATA.L _no_reg_exception	;  34, 0x22
	.DATA.L _no_reg_exception	;  35, 0x23
	.DATA.L _no_reg_exception	;  36, 0x24
	.DATA.L _no_reg_exception	;  37, 0x25
	.DATA.L _no_reg_exception	;  38, 0x26
	.DATA.L _no_reg_exception	;  39, 0x27
	.DATA.L _no_reg_exception	;  40, 0x28
	.DATA.L _no_reg_exception	;  41, 0x29
	.DATA.L _no_reg_exception	;  42, 0x2a
	.DATA.L _no_reg_exception	;  43, 0x2b
	.DATA.L _no_reg_exception	;  44, 0x2c
	.DATA.L _no_reg_exception	;  45, 0x2d
	.DATA.L _no_reg_exception	;  46, 0x2e
	.DATA.L _no_reg_exception	;  47, 0x2f
	.DATA.L _no_reg_exception	;  48, 0x30
	.DATA.L _no_reg_exception	;  49, 0x31
	.DATA.L _no_reg_exception	;  50, 0x32
	.DATA.L _no_reg_exception	;  51, 0x33
	.DATA.L _no_reg_exception	;  52, 0x34
	.DATA.L _no_reg_exception	;  53, 0x35
	.DATA.L _no_reg_exception	;  54, 0x36
	.DATA.L _no_reg_exception	;  55, 0x37
	.DATA.L _no_reg_exception	;  56, 0x38
	.DATA.L _no_reg_exception	;  57, 0x39
	.DATA.L _no_reg_exception	;  58, 0x3a
	.DATA.L _no_reg_exception	;  59, 0x3b
	.DATA.L _no_reg_exception	;  60, 0x3c
	.DATA.L _no_reg_exception	;  61, 0x3d
	.DATA.L _no_reg_exception	;  62, 0x3e
	.DATA.L _no_reg_exception	;  63, 0x3f
	.DATA.L _no_reg_exception	;  64, 0x40
	.DATA.L _no_reg_exception	;  65, 0x41
	.DATA.L _no_reg_exception	;  66, 0x42
	.DATA.L _no_reg_exception	;  67, 0x43
	.DATA.L _no_reg_exception	;  68, 0x44
	.DATA.L _no_reg_exception	;  69, 0x45
	.DATA.L _no_reg_exception	;  70, 0x46
	.DATA.L _no_reg_exception	;  71, 0x47
	.DATA.L _no_reg_exception	;  72, 0x48
	.DATA.L _no_reg_exception	;  73, 0x49
	.DATA.L _no_reg_exception	;  74, 0x4a
	.DATA.L _no_reg_exception	;  75, 0x4b
	.DATA.L _no_reg_exception	;  76, 0x4c
	.DATA.L _no_reg_exception	;  77, 0x4d
	.DATA.L _no_reg_exception	;  78, 0x4e
	.DATA.L _no_reg_exception	;  79, 0x4f
	.DATA.L _no_reg_exception	;  80, 0x50
	.DATA.L _no_reg_exception	;  81, 0x51
	.DATA.L _no_reg_exception	;  82, 0x52
	.DATA.L _no_reg_exception	;  83, 0x53
	.DATA.L __kernel_h8s_sci1_isr_error_entry	;  84, 0x54
	.DATA.L __kernel_h8s_sci1_isr_in_entry	;  85, 0x55
	.DATA.L __kernel_h8s_sci1_isr_out_entry	;  86, 0x56
	.DATA.L _no_reg_exception	;  87, 0x57
	.DATA.L _no_reg_exception	;  88, 0x58
	.DATA.L _no_reg_exception	;  89, 0x59
	.DATA.L _no_reg_exception	;  90, 0x5a
	.DATA.L _no_reg_exception	;  91, 0x5b

;
;  割込みの入口処理の定義
;    このファイルにはユーザー定義の割込みハンドラ（C言語ルーチン）名を
;    記述する
;
        .SECTION P, CODE, ALIGN=2

;
;    割込みの入口処理を生成するマクロの使い方
;      （割込み要因毎に異なる部分）
;
;    マクロINTHDR_ENTRY C_ROUTINE
;      パラメータ
;         C_ROUTINE：C言語ルーチンの関数名（先頭の'_'は付けない）
;

 INTHDR_ENTRY timer_handler
 INTHDR_ENTRY h8s_sci1_isr_error
 INTHDR_ENTRY h8s_sci1_isr_in
 INTHDR_ENTRY h8s_sci1_isr_out

 .END
