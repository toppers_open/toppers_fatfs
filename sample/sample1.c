/*
 *  FatFs for TOPPERS
 *      FAT File system/
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 * 
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2004 by Embedded and Real-Time Systems Laboratory
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2007- by Industrial Technology Institute,
 *                              Miyagi Prefectural Government, JAPAN
 * 
 *  上記著作権者は，以下の (1)〜(4) の条件か，Free Software Foundation 
 *  によって公表されている GNU General Public License の Version 2 に記
 *  述されている条件を満たす場合に限り，本ソフトウェア（本ソフトウェア
 *  を改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，その適用可能性も
 *  含めて，いかなる保証も行わない．また，本ソフトウェアの利用により直
 *  接的または間接的に生じたいかなる損害に関しても，その責任を負わない．
 * 
 */

/*--------------------------------------------------------------------------/
/  FatFs - FAT file system module  R0.04                     (C)ChaN, 2007
/---------------------------------------------------------------------------/
/ FatFs module is an experimenal project to implement FAT file system to
/ cheap microcontrollers. This is a free software and is opened for education,
/ research and development under license policy of following trems.
/
/  Copyright (C) 2007, ChaN, all right reserved.
/
/ * The FatFs module is a free software and there is no warranty.
/ * You can use, modify and/or redistribute it for personal, non-profit or
/   profit use without any restriction under your responsibility.
/ * Redistributions of source code must retain the above copyright notice.
/---------------------------------------------------------------------------*/

/* 
 *  ファイルシステムのテストプログラム
 * 
 * フルセット版FatFs（ff.c, ff.h）のAPIの動作テストを行っている。記録メ
 * ディアは、FAT16またはFAT32でフォーマットされ、まだ、ファイルやディレ
 * クトリが書き込まれていない状態を想定している。
 * テスト項目を以下に示す。
 * 
 * ・f_write()のテスト
 * 　ファイルdata.txtを書き込みモードでオープンする。
 * 　クラスタ境界をまたいだ書き込みを行う。
 * 　ファイルdata.txtをクローズする。
 * 
 * ・f_read()のテスト
 * 　ファイルdata.txtを読み出しモードでオープンする。
 * 　クラスタ境界をまたいだ読み出しを行い、期待値と比較を行う。
 * 
 * ・f_lseek()のテスト
 * 　ファイルdata.txtを読み出しモードでオープンする。
 * 　第2クラスタの先頭セクタにシークする。
 * 　シークした位置から読み出しを行い、期待値と比較する。
 * 
 * ・f_mkdir(), f_opendir()のテスト
 * 　/sub1, /sub1/sub2, sub1/sub2/sub3の順にディレクトリを作成し、
 * 　開けることを確認する。
 * 
 * ・f_readdir()のテスト
 * 　メディアに記録されているすべてのファイル名とディレクトリ名を再帰
 * 　的に読み出す。
 * 　/data.txt, /sub1, /sub1/sub2, sub1/sub2/sub3が読み出せれば、成功。
 * 
 * ・f_chmod(), f_stat()のテスト
 * 　ファイルstat.txtを作成する。
 * 　stat.txtのファイル属性を読み出す。
 * 　stat.txtに読み取り専用のファイル属性を設定する。
 * 　stat.txtのファイル属性を再度、読み出し、読み取り専用のファイル属
 * 　性が設定されていることを確認する。（※）
 * 
 * 　　※　記録メディアが脱着可能な場合はパソコン上でもファイル属性を
 * 　　　　確認する。
 * 　
 * ・f_unlink()のテスト
 * 　ファイルunlink.txtを作成する。
 * 　unlink.txtを削除する。
 * 　unlink.txtが削除されていることを確認する。（※）
 * 
 * 　　※　記録メディアが脱着可能な場合はパソコン上でもファイルが削除
 * 　　　　されていることを確認する。
 * 
 * ・f_rename()のテスト
 * 　ファイルold.txtを作成する。
 * 　old.txtをnew.txtにリネームする。
 * 　old.txtが存在しないことを確認する。（※）
 * 　new.txtが存在することを確認する。　（※）
 * 
 * 　　※　記録メディアが脱着可能な場合はパソコン上でもファイルが
 * 　　　　リネームされていることを確認する。
 * 
 * ・f_getfree()のテスト
 * 　f_getfree()で空きクラスタ数を求める。
 * 　空きクラスタ数をバイト数に換算して、ログ表示する。（※）
 * 　
 * 　　※　記録メディアが脱着可能な場合はパソコン上でも空き容量を
 * 　　　　確認する。
 * 
 * テストが成功した場合のログを以下に示す。
 * 
 * System logging task is started on port 1.
 * Sample program starts (exinf = 0).
 * test write: OK
 * test read: OK
 * test seek: OK
 * test mkdir: OK
 * Dir: 0:/
 * File: 0:/data.txt
 * Dir: 0:/sub1/
 * Dir: 0:/sub1/sub2/
 * Dir: 0:/sub1/sub2/sub3/
 * Check read only attribute of file 0:/stat.txt by PC.
 * test stat: OK
 * test rename: OK
 * test unlink: OK
 * Waiting for f_getfree() .....
 * 2043789312(0x79d1c000) bytes available on the disk.
 * 　→この数値は、使用する記録メディアのサイズによって異なる。
 * Check free space by PC.
 * test getfree: OK
 * -- buffered messages --
 * Sample program ends.
 * 
 */

#include <stdio.h> 
#include <string.h> 
#include <limits.h>

#include <t_services.h>
#include "kernel_id.h"
#include "sample1.h"
#include "ff.h"

#define DRIVE0		0u		/*  ドライブ番号  */
#define FDISK_FORMAT	0u		/*  フォーマット種別  */

#if 1
#define ROOT_DIR	"0:"
#else
#define ROOT_DIR	"1:"
#endif

#define FILENAME	ROOT_DIR "/data.txt"
#define STAT_FILENAME	ROOT_DIR "/stat.txt"
#define UNLNK_FILENAME	ROOT_DIR "/unlink.txt"
#define OLD_FILENAME	ROOT_DIR "/old.txt"
#define NEW_FILENAME	"new.txt"	/*  ドライブ番号はいらない  */

#define SUB_DIR1	ROOT_DIR "/sub1"
#define SUB_DIR2	ROOT_DIR "/sub1/sub2"
#define SUB_DIR3	ROOT_DIR "/sub1/sub2/sub3"

#define SECTOR_SIZE	512UL	/*  セクタ・サイズ（バイト単位）  */
#define CLUSTER_SIZE	4U	/*  クラスタ・サイズ（セクタ単位）  */
#define WAIT_SYSLOG	500	/*  syslog()の表示待ち時間  */
#define WORD_MAX	0xffffUL	/*  WORD型の最大値  */

#define BUF_SIZE	300


const static char test_data[] = 
  "0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef"
  "0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef"
  "0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef"
  "0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef";

static char buff[BUF_SIZE + 1];

/*  ファイル・システムのワークエリア  */
static FATFS ActiveFatFsObj;

static void test_read_write(FATFS *fs);
static void test_seek(FATFS *fs);
static void test_mkdir(void);
static void test_dir(void);
static void scan_files (char* path);

static void make_file(char *filename);
static void test_stat(void);
static void test_rename(void);
static void test_unlink(void);
static void test_getfree(void);


/*
 *  メインタスク
 */
void main_task(VP_INT exinf)
{
	FRESULT result;
	FATFS   *fs;

	vmsk_log(LOG_UPTO(LOG_INFO), LOG_UPTO(LOG_EMERG));
	syslog(LOG_NOTICE, "Sample program starts (exinf = %d).", exinf);

	syscall(serial_ctl_por(TASK_PORTID,
			(IOCTL_CRLF | IOCTL_FCSND | IOCTL_FCRCV)));

	/*
	 *  FATファイルシステムの初期化  
	 */
	fs = &ActiveFatFsObj;
	result = f_mount(DRIVE0, fs);
	assert(result == FR_OK);
	
	/*
	 *  各種テスト関数の呼び出し
	 */
	test_read_write(fs);
	test_seek(fs);
	test_mkdir();
	test_dir();
	test_stat();
	test_rename();
	test_unlink();
	test_getfree();

	syslog(LOG_NOTICE, "Sample program ends.");
	kernel_exit();
}


/*
 *  書き込みと読み出しテスト
 *　　複数クラスタにまたがるようにする。
 *　　
 *　　引数
 *　　　FATFS *fs：ファイルシステムのワークエリアの先頭アドレス
 */
static 
void test_read_write(FATFS *fs)
{
	int	cmp;
	UINT	times, i;
	WORD	n;
	size_t	data_length, total;
	FIL	file_obj;
	FRESULT result;

	result = f_open(&file_obj, FILENAME, FA_WRITE | FA_OPEN_ALWAYS);
	assert(result == FR_OK);

	/*
	 *　書き込むデータサイズの計算
	 *　　・複数クラスタにまたがるようにする。
	 *　　・上記のf_open()呼び出しで、ワークエリアが初期化済みで
	 *　　　あることが保証される。
	 */
	data_length = strlen(test_data);
	assert(data_length <= WORD_MAX);
	total = (fs->sects_clust) * SECTOR_SIZE;
	assert((total / data_length) <= (INT_MAX - 2UL));
	times = (UINT)((total / data_length) + 2UL);
	
	/*  書き込みテスト  */
	for(i = 0; i < times; ++i) {
		result = f_write(&file_obj, test_data, (WORD)data_length, &n);
		assert(result == FR_OK);
		if (n != data_length) {
			syslog(LOG_NOTICE, "test write: Error!");
			dly_tsk(WAIT_SYSLOG);
			syslog(LOG_NOTICE, "   %dth trial is failed.", i);
			dly_tsk(WAIT_SYSLOG);
			syslog(LOG_NOTICE, "test read: Skip");
			dly_tsk(WAIT_SYSLOG);
			result = f_close(&file_obj);
			assert(result == FR_OK);
			return;
		}
	}
	syslog(LOG_NOTICE, "test write: OK");
	dly_tsk(WAIT_SYSLOG);

	result = f_close(&file_obj);
	assert(result == FR_OK);

	result = f_open(&file_obj, FILENAME, FA_READ);
	assert(result == FR_OK);

	/*  読み出しテスト  */
	for(i = 0; i < times; ++i) {
		buff[0] = '\0';
		buff[BUF_SIZE] = '\0';
		result = f_read(&file_obj, buff, (WORD)data_length, &n);
		assert(result == FR_OK);
		assert(n == data_length);
		cmp = strncmp(test_data, buff, data_length);
		assert(cmp == 0);
	}
	syslog(LOG_NOTICE, "test read: OK");
	dly_tsk(WAIT_SYSLOG);
	
	result = f_close(&file_obj);
	assert(result == FR_OK);
}

/*
 *  seekのテスト
 *　　複数クラスタにまたがるようにする。
 *　　
 *　　引数
 *　　　FATFS *fs：ファイルシステムのワークエリアの先頭アドレス
 */
static 
void test_seek(FATFS *fs)
{
	int	cmp;
	WORD	n;
	size_t	data_length;
	DWORD	cluster_size, offset;
	FIL	file_obj;
	FRESULT result;

	/*
	 *　シークするオフセットの計算
	 *　　第２クラスタに到達するようにする。
	 */
	data_length = strlen(test_data);
	cluster_size = (fs->sects_clust) * SECTOR_SIZE;
	offset = cluster_size - (cluster_size % data_length);

	result = f_open(&file_obj, FILENAME, FA_READ);
	assert(result == FR_OK);

	result = f_lseek(&file_obj, offset);
	assert(result == FR_OK);

	buff[0] = '\0';
	buff[BUF_SIZE] = '\0';
	result = f_read(&file_obj, buff, (WORD)data_length, &n);
	assert(result == FR_OK);
	if (n != data_length) {
		syslog(LOG_NOTICE, "test seek: Error(data length error)");
		dly_tsk(WAIT_SYSLOG);
		result = f_close(&file_obj);
		assert(result == FR_OK);
		return;
	}
	cmp = strncmp(test_data, buff, data_length);
	assert(cmp == 0);

	syslog(LOG_NOTICE, "test seek: OK");
	dly_tsk(WAIT_SYSLOG);
	
	result = f_close(&file_obj);
	assert(result == FR_OK);
}

static 
void test_mkdir(void)
{
	FRESULT result;
	DIR dirs;

	result = f_mkdir(SUB_DIR1);
	assert(result == FR_OK);
	result = f_opendir(&dirs, SUB_DIR1);
	assert(result == FR_OK);

	result = f_mkdir(SUB_DIR2);
	assert(result == FR_OK);
	result = f_opendir(&dirs, SUB_DIR2);
	assert(result == FR_OK);

	result = f_mkdir(SUB_DIR3);
	assert(result == FR_OK);
	result = f_opendir(&dirs, SUB_DIR3);
	assert(result == FR_OK);

	syslog(LOG_NOTICE, "test mkdir: OK");
	dly_tsk(WAIT_SYSLOG);
}

static 
void test_dir(void)
{
	strcpy(buff, ROOT_DIR);
	scan_files(buff);
}


/*
 *　指定されたディレクトリ以下のディレクトリとファイルを
 *　（再帰的に）すべて表示する。
 */
static 
void scan_files (char* path)
{
    FILINFO finfo;
    DIR dirs;
    size_t i;

    if (f_opendir(&dirs, path) == FR_OK) {
        syslog(LOG_NOTICE, "Dir: %s/", path);
	dly_tsk(WAIT_SYSLOG);

        i = strlen(path);
        while ((f_readdir(&dirs, &finfo) == FR_OK) && finfo.fname[0]) {
            if (finfo.fattrib & AM_DIR) {
                sprintf(path+i, "/%s", &finfo.fname[0]);
                scan_files(path);
                *(path+i) = '\0';
            } else {
                syslog(LOG_NOTICE, "File: %s/%s", path, &finfo.fname[0]);
		dly_tsk(WAIT_SYSLOG);
            }
        }
    }
}


/*
 *　テスト用にファイルを作る。
 */
static 
void make_file(char *filename)
{
	size_t	data_length;
	WORD	n;
	FIL	file_obj;
	FRESULT result;

	data_length = strlen(test_data);
	
	result = f_open(&file_obj, filename, FA_WRITE | FA_OPEN_ALWAYS);
	assert(result == FR_OK);

	result = f_write(&file_obj, test_data, (WORD)data_length, &n);
	assert(result == FR_OK);
	assert(n == data_length);

	result = f_close(&file_obj);
	assert(result == FR_OK);
}


void test_stat(void)
{
	FILINFO finfo;
	FRESULT result;

	make_file(STAT_FILENAME);
	/*  ファイルができていることの確認  */
	result = f_stat(STAT_FILENAME, &finfo);
	assert(result == FR_OK);

	// Set read-only flag , clear archive flag and others are not changed.
	result = f_chmod(STAT_FILENAME, AM_RDO, AM_RDO | AM_ARC);
	assert(result == FR_OK);

	result = f_stat(STAT_FILENAME, &finfo);
	assert(result == FR_OK);
	assert((finfo.fattrib & AM_RDO) != 0);

	syslog(LOG_NOTICE, "Check read only attribute of file %s by PC.",
		STAT_FILENAME);
	syslog(LOG_NOTICE, "test stat: OK");
	dly_tsk(WAIT_SYSLOG);
}

static 
void test_unlink(void)
{
	FILINFO finfo;
	FRESULT result;

	make_file(UNLNK_FILENAME);
	/*  ファイルができていることの確認  */
	result = f_stat(UNLNK_FILENAME, &finfo);
	assert(result == FR_OK);

	result = f_unlink(UNLNK_FILENAME);
	assert(result == FR_OK);

	/*  ファイルが削除されていることの確認  */
	result = f_stat(UNLNK_FILENAME, &finfo);
	assert(result == FR_NO_FILE);

	syslog(LOG_NOTICE, "test unlink: OK");
	dly_tsk(WAIT_SYSLOG);
}

static 
void test_rename(void)
{
	FILINFO finfo;
	FRESULT result;

	make_file(OLD_FILENAME);
	/*  ファイルができていることの確認  */
	result = f_stat(OLD_FILENAME, &finfo);
	assert(result == FR_OK);

	result = f_rename(OLD_FILENAME, NEW_FILENAME);
	assert(result == FR_OK);

	/*  ファイルがリネームされていることの確認  */
	result = f_stat(OLD_FILENAME, &finfo);
	assert(result == FR_NO_FILE);

	result = f_stat(NEW_FILENAME, &finfo);
	assert(result == FR_OK);

	syslog(LOG_NOTICE, "test rename: OK");
	dly_tsk(WAIT_SYSLOG);
}

static 
void test_getfree(void)
{
	DWORD clust, free_size;
	FATFS *fs;
	FRESULT result;

	// Get free clusters
	syslog(LOG_NOTICE, "Waiting for f_getfree() .....");
	result = f_getfree(ROOT_DIR, &clust, &fs);
	assert(result == FR_OK);

    	// Get free bytes
    	free_size = clust * (fs->sects_clust) * SECTOR_SIZE;
    	assert(sizeof(DWORD) <= sizeof(long));
	syslog(LOG_NOTICE, "%lu(0x%lx) bytes available on the disk.", 
		free_size, free_size);
	syslog(LOG_NOTICE, "Check free space by PC.");

	syslog(LOG_NOTICE, "test getfree: OK");
	dly_tsk(WAIT_SYSLOG);
}
